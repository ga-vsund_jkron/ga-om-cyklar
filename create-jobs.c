/*
create-jobs.c - 
*/

static void panic(char *format, ...);

#include "utility.c"

static void panic(char *format, ...){
  va_list list;
  
  fprintf(stderr, "\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "\n");
  fprintf(stderr, ANSI_COLOR_RED);
  fprintf(stderr, "[create-jobs.c ");
  fprint_current_date(stderr);
  fprintf(stderr, " FATAL ERROR] ");
  fprintf(stderr, ANSI_COLOR_RESET);
  va_start(list, format);
  vfprintf(stderr, format, list);
  va_end(list);
  fprintf(stderr, "\n");
  fflush(stderr);
  
  __asm__("int3");
  
  exit(1);
}

#define VERSION 8

__attribute__ ((format (printf, 1, 2)))
static int run_command(char *format_command, ...){
  va_list list;
  va_start(list, format_command);
  char buf[8192];
  vsnprintf(buf, sizeof(buf), format_command, list);
  fprintf(stderr, "Running command `%s`...\n", buf);
  int status = system(buf);
  va_end(list);
  int result;
  if(WIFEXITED(status)){
    result = WEXITSTATUS(status);
    if(result == 0){
      fprintf(stderr, "... command `%s` exited with status %d!\n", buf, result);
    }else{
      fprintf(stderr, "WARNING: ... command `%s` exited with nonzero status %d!\n", buf, result);
    }
  }else{
    result = 0xffffffff;
    fprintf(stderr, "WARNING: ... command `%s` did not exit cleanly!\n", buf);
  }
  return result;
}

typedef struct IterSpec3 IterSpec3;
struct IterSpec3{
  IterSpec values[3];
};

//
// Job file generation.
//

#define JOB_BATCH_SIZE (32)

typedef struct GenJobsState GenJobsState;
struct GenJobsState{
  char *name;
  int job_count;
} gen_jobs_state;

#define JOB_CREATION_TEMP_DIRECTORY "create-jobs-temp/"

static void begin_gen_jobs(char *name){
  gen_jobs_state = (GenJobsState){0};
  gen_jobs_state.name = name;
  mkdir(JOB_CREATION_TEMP_DIRECTORY, 0777);
}
static void end_gen_jobs(){
  char out_dir_name[4096];
  snprintf(out_dir_name, sizeof(out_dir_name), "%s_batches", gen_jobs_state.name);
  
  mkdir(out_dir_name, 0777);
  fiz((gen_jobs_state.job_count + JOB_BATCH_SIZE - 1) / JOB_BATCH_SIZE){
    fprintf(stderr, "Packing batch %d...\n", i);
    run_command("cd create-jobs-temp/batch%d/ && tar -czf ../../%s/%s_batch%d.tar.gz *",
                i, out_dir_name, gen_jobs_state.name, i);
  }
  
  run_command("rm -r \"%s\"", JOB_CREATION_TEMP_DIRECTORY);
  
  gen_jobs_state = (GenJobsState){0};
}

typedef struct JobParams JobParams;
struct JobParams{
  double d;
  double s;
  float bike_speed;
  int cyclist_count;
  
  IterSpec3 iter_specs;
  int maximum_simpleFoam_iterations;
  
  bool override_initial_conditions;
  float epsilon0;
  float k0;
  
  int box_max_extra[3];
  int box_min_extra[3];
};

typedef struct PushedJob PushedJob;
struct PushedJob{
  char name[1024];
  int job_number;
  int batch_number;
};

static PushedJob push_job(JobParams params){
  PushedJob result = {0};
  
  result.job_number = gen_jobs_state.job_count;
  gen_jobs_state.job_count += 1;
  
  result.batch_number = result.job_number / JOB_BATCH_SIZE;
  
  char batch_dir[1024];
  snprintf(batch_dir, sizeof(batch_dir), "%s/batch%d/", JOB_CREATION_TEMP_DIRECTORY, result.batch_number);
  mkdir(batch_dir, 0777);
  snprintf(result.name, sizeof(result.name), "job_%s_%04d_batch%03d.txt",
           gen_jobs_state.name, result.job_number, result.batch_number);
  char buf[2048];
  snprintf(buf, sizeof(buf), "%s/%s", batch_dir, result.name);
  FILE *f = fopen(buf, "w");
  if(!f){
    fprintf(stderr, "some directory that should exist does not exist :( (tried to open `%s` but failed)\n", buf);
    exit(1);
  }
  fprintf(f, "version %d\n", VERSION);
  fprintf(f, "cyclist-count %d\n", params.cyclist_count);
  fprintf(f, "distance %f\n", params.d);
  fprintf(f, "y-offset %f\n", params.s);
  fprintf(f, "bike-speed %f\n", params.bike_speed);
  fprintf(f, "maximum-simpleFoam-iterations %d\n", params.maximum_simpleFoam_iterations);
  fprintf(f, "step-iter-counts");
  fiz(3){
    if(params.iter_specs.values[i].kind == ITER_SPEC_fixed){
      fprintf(f, " fixed %d", params.iter_specs.values[i].fixed);
    }else if(params.iter_specs.values[i].kind == ITER_SPEC_converge){
      fprintf(f, " converge %f", params.iter_specs.values[i].converge);
    }
  }
  fprintf(f, "\n");
  
  if(params.override_initial_conditions){
    fprintf(f, "initial-override epsilon0 %.16f k0 %.16f\n", params.epsilon0, params.k0);
  }else{
    fprintf(f, "# Uncomment the line below to override the epsilon0 and k0 values\n");
    fprintf(f, "# initial-override epsilon0 <epsilon0 value> k0 <k0 value>\n");
  }
  fprintf(f, "issued "); fprint_current_date(f); fprintf(f, "\n");
  
  fprintf(f, "box-min-extra %d %d %d\n", params.box_min_extra[0], params.box_min_extra[1], params.box_min_extra[2]);
  fprintf(f, "box-max-extra %d %d %d\n", params.box_max_extra[0], params.box_max_extra[1], params.box_max_extra[2]);
  
  fclose(f);
  
  return result;
}

//
// LHS
//

#define DISTRIBUTION_uniform 1
#define DISTRIBUTION_triangular 2
#define DISTRIBUTION_exponential 3
#define DISTRIBUTION_normal 4

static void random_permutation(int *values, int count){
  bool *temp = calloc(count, sizeof(bool));
  fiz(count){
    int value;
    while(value = random_i32_ab(0, count - 1), temp[value]){}
    temp[value] = true;
    values[i] = value;
  }
  free(temp);
}

#define LHS_N 1024


static double inverse_exponential_cdf(double p){
  double l = 3.2;
  double result;
  if(0.999 - p > 0){
    result = (1 - exp(-l)) * -log(1 - p) / l;
  }else{
    result = INFINITY;
  }
  return result;
}

static double inverse_normal_cdf(double p){
  // NOTE(jkron - 2020-02-10): Based on https://web.archive.org/web/20150811000355/http://home.online.no/~pjacklam/notes/invnorm
  
  double a1 = -3.969683028665376e+01;
  double a2 =  2.209460984245205e+02;
  double a3 = -2.759285104469687e+02;
  double a4 =  1.383577518672690e+02;
  double a5 = -3.066479806614716e+01;
  double a6 =  2.506628277459239e+00;
  
  double b1 = -5.447609879822406e+01;
  double b2 =  1.615858368580409e+02;
  double b3 = -1.556989798598866e+02;
  double b4 =  6.680131188771972e+01;
  double b5 = -1.328068155288572e+01;
  
  double c1 = -7.784894002430293e-03;
  double c2 = -3.223964580411365e-01;
  double c3 = -2.400758277161838e+00;
  double c4 = -2.549732539343734e+00;
  double c5 =  4.374664141464968e+00;
  double c6 =  2.938163982698783e+00;
  
  double d1 =  7.784695709041462e-03;
  double d2 =  3.224671290700398e-01;
  double d3 =  2.445134137142996e+00;
  double d4 =  3.754408661907416e+00;
  
  double central_begin = 0.02425;
  double central_end  = 1 - 0.02425;
  
  double epsilon = 0.000001;
  if(p < epsilon){
    p = epsilon;
  }
  if(p > 1 - epsilon){
    p = 1 - epsilon;
  }
  
  double result;
  if(p < central_begin){
    double q = sqrt(-2 * log(p));
    result = 
      (((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) /
      ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
  }else if(p <= central_end){
    double q = p - 0.5;
    double r = q * q;
    result = (((((a1 * r + a2) * r + a3) * r + a4) * r + a5) * r + a6) * q /
      (((((b1 * r + b2) * r + b3) * r + b4) * r + b5) * r + 1);
  }else{
    double q = sqrt(-2 * log(1 - p));
    result = -(((((c1 * q + c2) * q + c3) * q + c4) * q + c5) * q + c6) /
      ((((d1 * q + d2) * q + d3) * q + d4) * q + 1);
  }
  return result;
}

static double inverse_triangular_cdf(double p){
  double a = 0;
  double b = 1;
  double c = 0;
  double result = (-sqrt((1 - p) * (b - a) * (b - c)) + b);
  return result;
}

typedef struct LHSVariable LHSVariable;
struct LHSVariable{
  double points[LHS_N + 1];
  double values[LHS_N];
  int permutation[LHS_N];
};

static double map01_to_ab(double value01, double a, double b){
  double result = value01 * (b - a) + a;
  return result;
}

static LHSVariable compute_variable(int distribution, double lower, double upper){
  LHSVariable result = {0};
  switch(distribution){
    case DISTRIBUTION_uniform:{
      result.points[0] = map01_to_ab(0, lower, upper);
      for(int point = 0; point < LHS_N; ++point){
        result.points[point + 1] = map01_to_ab((double)(point + 1) / (double)LHS_N, lower, upper);
        result.values[point] = random_f64_ab(result.points[point], result.points[point + 1]);
      }
    }break;
    
    case DISTRIBUTION_triangular:{
      result.points[0] = map01_to_ab(inverse_triangular_cdf(0), lower, upper);
      for(int point = 0; point < LHS_N; ++point){
        double interval_begin = point / (double)(LHS_N);
        double interval_end = (point + 1) / (double)(LHS_N);
        result.points[point + 1] = map01_to_ab(inverse_triangular_cdf(interval_end), lower, upper);
        result.values[point] = map01_to_ab(inverse_triangular_cdf(random_f64_ab(interval_begin, interval_end)), lower, upper);
      }
    }break;
    
    case DISTRIBUTION_normal:{
      result.points[0] = inverse_normal_cdf(0);
      for(int point = 0; point < LHS_N; ++point){
        double interval_begin = point / (double)(LHS_N);
        double interval_end = (point + 1) / (double)(LHS_N);
        result.points[point + 1] = inverse_normal_cdf(interval_end);
        result.values[point] = inverse_normal_cdf(random_f64_ab(interval_begin, interval_end));
      }
    }break;
  }
  
  for(int point = 0; point < LHS_N; ++point){
    if(result.points[point] > result.values[point] || result.points[point + 1] < result.values[point]){
      fprintf(stderr, "[create-jobs.c] When generating LHS variable a value was not bounded by its interval.\n");
      assert(!"unbounded value!");
      exit(1);
    }
  }
  
  random_permutation(result.permutation, array_count(result.permutation));
  return result;
}

int main(int argc, char **argv){
  IterSpec3 iter_specs = {
    .values = {
      { .kind = ITER_SPEC_fixed, .fixed = 200 },
      { .kind = ITER_SPEC_fixed, .fixed = 100 },
      { .kind = ITER_SPEC_converge, .converge = 0.2 },
    },
  };
  int maximum_simpleFoam_iterations = 2000;
  
  {
    fprintf(stderr, "Generating LHS...\n");
    LHSVariable d       = compute_variable(DISTRIBUTION_triangular, 0, 10);
    LHSVariable s       = compute_variable(DISTRIBUTION_normal, /*OBS: Dessa ignoreras helt!*/ -1, 1);
    LHSVariable v_cykel = compute_variable(DISTRIBUTION_uniform, 20/3.6, 50/3.6);
    
    begin_gen_jobs("lhs");
    
    {
      FILE *lhs_values = fopen("lhs_values.csv", "w");
      fprintf(lhs_values,
              "i\t"
              "d interval begin\td value\td interval end\t"
              "s interval begin\ts value\ts interval end\t"
              "v_cykel interval begin\tv_cykel value\tv_cykel interval end\n");
      fiz(LHS_N){
        fprintf(lhs_values, "%d\t", i);
        fprintf(lhs_values, "%f\t%f\t%f\t", d.points[i], d.values[i], d.points[i + 1]);
        fprintf(lhs_values, "%f\t%f\t%f\t", s.points[i], s.values[i], s.points[i + 1]);
        fprintf(lhs_values, "%f\t%f\t%f\n", v_cykel.points[i], v_cykel.values[i], v_cykel.points[i + 1]);
      }
      fclose(lhs_values);
    }
    
    {
      FILE *lhs_index = fopen("lhs_job_index.csv", "w");
      fprintf(lhs_index, "job name\tnumber\tbatch number\td coord\td value\tts coord\ts value\tv_cykel coord\tv_cykel value\n");
      fiz(LHS_N){
        JobParams params = {
          .d = d.values[d.permutation[i]],
          .s = s.values[s.permutation[i]],
          .bike_speed = v_cykel.values[v_cykel.permutation[i]],
          .cyclist_count = 2,
          
          .iter_specs = iter_specs,
          .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
        };
        
        PushedJob pushed = push_job(params);
        fprintf(lhs_index, "%s\t%d\t%d\t"
                "%d\t%f\t" "%d\t%f\t" "%d\t%f\n",
                pushed.name, pushed.job_number, pushed.batch_number,
                d.permutation[i], d.values[d.permutation[i]],
                s.permutation[i], s.values[s.permutation[i]],
                v_cykel.permutation[i], v_cykel.values[v_cykel.permutation[i]]);
      }
      fclose(lhs_index);
    }
    
    end_gen_jobs();
  }
  
  {
    double fixed_d = 5;
    double fixed_s = 0;
    double fixed_bike_speed = 7;
    double fixed_epsilon0 = 0.004276;
    double fixed_k0 = 0.076147;
    
    fprintf(stderr, "Generating tests...\n");
    
    begin_gen_jobs("test_default");
    // One with normal settings, one with high mesh resolution.
    fiz(2){
      push_job((JobParams){
               .d = fixed_d,
               .s = fixed_s,
               .bike_speed = fixed_bike_speed,
               .cyclist_count = 2,
               .iter_specs = iter_specs,
               .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
               });
    }
    end_gen_jobs();
    
    begin_gen_jobs("test_sensitivity_epsilon");
    fiz(16){
      float epsilon0 = map01_to_ab((double)i / (16 - 1), fixed_epsilon0 * 0.5, fixed_epsilon0 * 1.5);
      
      JobParams params = {
        .d = fixed_d,
        .s = fixed_s,
        .bike_speed = fixed_bike_speed,
        .cyclist_count = 2,
        .iter_specs = iter_specs,
        .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
        
        .override_initial_conditions = true,
        .epsilon0 = epsilon0,
        .k0 = fixed_k0,
      };
      
      push_job(params);
    }
    end_gen_jobs();
    
    begin_gen_jobs("test_sensitivity_k");
    fiz(16){
      float k0 = map01_to_ab((double)i / (16 - 1), fixed_k0 * 0.5, fixed_k0 * 1.5);
      
      JobParams params = {
        .d = fixed_d,
        .s = fixed_s,
        .bike_speed = fixed_bike_speed,
        .cyclist_count = 2,
        .iter_specs = iter_specs,
        .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
        
        .override_initial_conditions = true,
        .epsilon0 = fixed_epsilon0,
        .k0 = k0,
      };
      
      push_job(params);
    }
    end_gen_jobs();
    
    begin_gen_jobs("test_bike_speed");
    fiz(8){
      float bike_speed = map01_to_ab((double)i / (8 - 1), 20 / 3.6, 40 / 3.6);
      
      JobParams params = {
        .d = fixed_d,
        .s = fixed_s,
        .bike_speed = bike_speed,
        .cyclist_count = 2,
        .iter_specs = iter_specs,
        .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
      };
      
      push_job(params);
    }
    end_gen_jobs();
    
    begin_gen_jobs("test_d");
    fiz(8){
      float d = map01_to_ab((double)i / (8 - 1), 0, 16);
      
      JobParams params = {
        .d = d,
        .s = fixed_s,
        .bike_speed = fixed_bike_speed,
        .cyclist_count = 2,
        .iter_specs = iter_specs,
        .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
      };
      
      push_job(params);
    }
    end_gen_jobs();
    
    begin_gen_jobs("test_s");
    fiz(8){
      float s = map01_to_ab((double)i / (8 - 1), -1.5, 1.5);
      
      JobParams params = {
        .d = fixed_d,
        .s = s,
        .bike_speed = fixed_bike_speed,
        .cyclist_count = 2,
        .iter_specs = iter_specs,
        .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
      };
      
      push_job(params);
    }
    end_gen_jobs();
    
    begin_gen_jobs("test_box_size");
    fiz(8){
      int box_extra[8][6] = {
        { 20, 0,  0 , 0 , 0 , 0  }, // 0 longer in front.
        { 0 , 20, 0 , 0 , 20, 0  }, // 1 wider.
        { 0 , 0 , 0 , 0 , 0 , 20 }, // 2 higher to the ceiling.
        { 20, 20, 0 , 20, 20, 20 }, // 3 bigger overall
        { 50, 0 , 0 , 50, 0 , 0  }, // 4 extremely long.
        { 0 , 50, 0 , 0 , 50, 0  }, // 5 extremely wide
        { 0 , 0 , 50, 0 , 0 , 0  }, // 6 displaced floor
        { 50, 50, 0 , 50, 50,100 }, // 7 extreme size
      };
      
      JobParams params = {
        .d = fixed_d,
        .s = fixed_s,
        .bike_speed = fixed_bike_speed,
        .cyclist_count = 2,
        .iter_specs = iter_specs,
        .maximum_simpleFoam_iterations = maximum_simpleFoam_iterations,
        
        .box_min_extra[0] = box_extra[i][0],
        .box_min_extra[1] = box_extra[i][1],
        .box_min_extra[2] = box_extra[i][2],
        .box_max_extra[0] = box_extra[i][3],
        .box_max_extra[1] = box_extra[i][4],
        .box_max_extra[2] = box_extra[i][5],
      };
      
      push_job(params);
    }
    end_gen_jobs();
  }
  
  return(0);
}
