#!/bin/sh
cd ${0%/*} || exit 1                        # Run from this directory
source /opt/OpenFOAM/OpenFOAM-v1906/etc/bashrc
. $WM_PROJECT_DIR/bin/tools/RunFunctions    # Tutorial run functions

if [[ ! -f log.simpleFoam ]] ; then
echo "[prepare_for_paraview.sh] It looks like the simulation has not yet been run! Please run run_simulation.sh before running this script"
exit 1
fi

runApplication reconstructParMesh -constant -mergeTol 1e-6
runApplication reconstructPar

