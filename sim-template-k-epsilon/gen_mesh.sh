#!/bin/sh
# gen_mesh.sh - Generates the OpenFOAM mesh. Should be run in docker.

cd ${0%/*} || exit 1                        # Run from this directory
source /opt/OpenFOAM/OpenFOAM-v1906/etc/bashrc
. $WM_PROJECT_DIR/bin/tools/RunFunctions

echo $$ > inside_docker.pid

# Make dummy 0 directory
mkdir 0

runApplication blockMesh
# \cp system/decomposeParDict.hierarchical system/decomposeParDict
runApplication decomposePar -decomposeParDict system/decomposeParDict.hierarchical

# \cp system/decomposeParDict.ptscotch system/decomposeParDict
runParallel snappyHexMesh -decomposeParDict system/decomposeParDict.ptscotch -profiling -overwrite
if [[ ! $? -eq 0 ]] ;then 
  echo "[gen_mesh.sh] snappyHexMesh failed! Fatal error! Exiting now!!!!!"
  exit 1
fi

find . -iname '*level*' -type f -delete

echo "[gen_mesh.sh] Done generating mesh! Noting size"
du -hcs .

rm inside_docker.pid

