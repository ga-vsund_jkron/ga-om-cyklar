#!/bin/sh

cd ${0%/*} || exit 1                        # Run from this directory
source /opt/OpenFOAM/OpenFOAM-v1906/etc/bashrc
. $WM_PROJECT_DIR/bin/tools/RunFunctions

echo $$ > inside_docker.pid

runParallel snappyHexMesh -decomposeParDict system/decomposeParDict.ptscotch -profiling -overwrite
exit_code=$?
if [[ ! $exit_code -eq 0 ]] ;then 
  echo "[script_snappyHexMesh.sh] snappyHexMesh returned nonzero exit code"
  exit $exit_code
fi

# NOTE(jkron - 2020-01-28): I have no idea what these level files are but this was
# in the tutorial scripts, so lets keep it...
find . -iname '*level*' -type f -delete

# NOTE(jkron - 2020-01-28): Not entirely sure what this one does either...
restore0Dir -processor

rm inside_docker.pid

