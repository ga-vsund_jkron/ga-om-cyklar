#!/bin/sh
# run_simulation.sh - Runs the simulation using the previously generated mesh. Should be run within the docker container.

echo $$ > inside_docker.pid

cd ${0%/*} || exit 1                        # Run from this directory
source /opt/OpenFOAM/OpenFOAM-v1906/etc/bashrc
. $WM_PROJECT_DIR/bin/tools/RunFunctions

if [[ ! -f log.snappyHexMesh ]] ; then
echo "[run_simulation.sh] It looks like the mesh has not been generated. gen_mesh.sh has to be run first."
exit 1
fi

# Set the initial fields
restore0Dir -processor

runParallel renumberMesh -overwrite
runParallel potentialFoam -initialiseUBCs
runParallel checkMesh -writeFields '(nonOrthoAngle)' -constant
runParallel $(getApplication)

rm inside_docker.pid

#------------------------------------------------------------------------------
