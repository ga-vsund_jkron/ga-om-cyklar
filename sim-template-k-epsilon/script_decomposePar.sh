#!/bin/sh

cd ${0%/*} || exit 1                        # Run from this directory
  source /opt/OpenFOAM/OpenFOAM-v1906/etc/bashrc
  . $WM_PROJECT_DIR/bin/tools/RunFunctions
  
  echo $$ > inside_docker.pid
  
  runApplication decomposePar -decomposeParDict system/decomposeParDict.hierarchical
  exit_code=$?
  if [[ ! $exit_code -eq 0 ]] ;then 
    echo "[script_decomposePar.sh] decomposePar returned nonzero exit code"
    exit $exit_code
  fi
  
  rm inside_docker.pid
  
  