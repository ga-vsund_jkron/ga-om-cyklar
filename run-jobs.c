/*
run-jobs.c - Runs a set of jobs (from the 'pending-jobs/' directory and 'batches/' directory) by preparing the corresponding simulations, running them and extracting data from them.
Author: jkron

*/

static void panic(char *format, ...);

#include "utility.c"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "graphite.c"

static bool ctrl_c_detected;

struct{
  FILE *file;
} _log;
static void shutdown(int status){
  if(_log.file){
    fclose(_log.file);
    _log.file = 0;
  }
  exit(status);
}
#define exit !!!!!!!!!!!!!! DONT USE THIS! !!!!!!!!!!!!

__attribute__ ((format (printf, 1, 2)))
static void log_info(char *format, ...){
  va_list list;
  
  FILE *outputs[] = { stderr, _log.file };
  fiz(array_count(outputs)){
    FILE *out = outputs[i];
    if(out){
      if(out == stderr){ fprintf(out, ANSI_COLOR_GREEN); }
      fprintf(out, "[run-jobs.c ");
      fprint_current_date(out);
      fprintf(out, " INFO] ");
      if(out == stderr){ fprintf(out, ANSI_COLOR_RESET); }
      va_start(list, format);
      vfprintf(out, format, list);
      va_end(list);
      fprintf(out, "\n");
      fflush(out);
    }
  }
}
__attribute__ ((format (printf, 1, 2)))
static void log_warning(char *format, ...){
  va_list list;
  
  FILE *outputs[] = { stderr, _log.file };
  fiz(array_count(outputs)){
    FILE *out = outputs[i];
    if(out){
      if(out == stderr){ fprintf(out, ANSI_COLOR_YELLOW); }
      fprintf(out, "[run-jobs.c ");
      fprint_current_date(out);
      fprintf(out, " WARN] ");
      if(out == stderr){ fprintf(out, ANSI_COLOR_RESET); }
      va_start(list, format);
      vfprintf(out, format, list);
      va_end(list);
      fprintf(out, "\n");
      fflush(out);
    }
  }
}


FILE *work_shift_csv;
char *worker_name;
char *auto_directory_path;

static void write_work_shift_entry(char *operation, char *data){
  fprint_current_date(work_shift_csv);
  fprintf(work_shift_csv, "\t%ld\t%s\t%s\t%s\n", time(NULL), worker_name, operation, data);
  fflush(work_shift_csv);
}


static void panic(char *format, ...){
  va_list list;
  
  if(work_shift_csv){
    write_work_shift_entry("fatal error", "");
    fclose(work_shift_csv);
    work_shift_csv = NULL;
  }
  
  FILE *outputs[] = { stderr, _log.file };
  fiz(array_count(outputs)){
    FILE *out = outputs[i];
    if(out){
      fprintf(out, "\n");
      fprintf(out, "\n");
      fprintf(out, "\n");
      if(out == stderr){ fprintf(out, ANSI_COLOR_RED); }
      fprintf(out, "[run-jobs.c ");
      fprint_current_date(out);
      fprintf(out, " FATAL ERROR] ");
      if(out == stderr){ fprintf(out, ANSI_COLOR_RESET); }
      va_start(list, format);
      vfprintf(out, format, list);
      va_end(list);
      fprintf(out, "\n");
      fflush(out);
    }
  }
  shutdown(1);
}

// NOTE: The names of the components present in the .obj-file.
// Exposed here in order to be used for measuring forces in the
// postprocessing stage on each individual component.
static char *object_names[] = {
  "Cyklist",
  "Cykel"
};
static char *group_names[] = {
  "front_", "back_"
};

static float bike_length = 1.8; // NOTE: Distance from the furthest points of the back wheel and the front wheel, i.e. the distance across the bike

static void duplicate_obj(char *input_path, char *output_path, float distance, float y_offset, int cyclist_count){
  struct v3{ float x, y, z; };
  struct face{ int count; int v[4]; int n[4]; }; // NOTE: .obj uses 1-indexing!!!
  
  int max_vertex_count = 1024*1024*80;
  struct v3 *vertices = malloc(sizeof(struct v3) * max_vertex_count);
  int vertex_count = 0;
  int max_normals_count = 1024*1024*80;
  struct v3 *normals = malloc(sizeof(struct v3) * max_normals_count);
  int normals_count = 0;
  int max_face_count = 1024*1024*80;
  struct face *faces = malloc(sizeof(struct face) * max_face_count);
  int face_count = 0;
  
  if(!vertices) panic("Out of memory (when trying to allocate vertices in duplicate_obj)");
  if(!normals) panic("Out of memory (when trying to allocate normals in duplicate_obj)");
  if(!faces) panic("Out of memory (when trying to allocate faces in duplicate_obj)");
  
  struct object{
    int kind;
    int vertex_begin;
    int normal_begin;
    int face_begin;
  };
  
  struct object objects[256] = {0};
  int object_count = 0;
  
  {
    FILE *f = fopen(input_path, "r");
    if(!f){
      panic("duplicate_obj was unable to open the file `%s`", input_path);
    }
    
    char *line = malloc(8192);
    size_t line_size = 8192;
    
    while(getline(&line, &line_size, f) != -1){
      if(line[0] != '\0' && line[strlen(line) - 1] == '\n'){ line[strlen(line) - 1] = '\0'; }
      
      if(line[0] != '#' && line[0] != '\0'){
        char *parts[256];
        int part_count = split_on_whitespace(parts, 256, line, false);
        if(string_equals(parts[0], "v") && part_count == 4){
          struct v3 vertex = {
            atof(parts[1]),
            atof(parts[2]),
            atof(parts[3]),
          };
          if(vertex_count >= max_vertex_count){
            panic("duplicate_obj tried to read in too many vertices.");
          }
          vertices[vertex_count++] = vertex;
        }else if(string_equals(parts[0], "vn") && part_count == 4){
          struct v3 normal = {
            atof(parts[1]),
            atof(parts[2]),
            atof(parts[3]),
          };
          if(normals_count >= max_normals_count){
            panic("duplicate_obj triet to read in too many normals");
          }
          normals[normals_count++] = normal;
        }else if(string_equals(parts[0], "f") && part_count >= 4){
          struct face f = {0};
          f.count = part_count - 1;
          if(f.count > 4){
            panic("duplicate_obj encountered a `f A[/B][/C] A[/B][/C] A[/B][/C] A[/B][/C]` pattern in the .obj file but there are more than 4 `A[/B][/C]` things.");
          }
          for(int i = 0; i < f.count; ++i){
            char *str = parts[i + 1];
            f.v[i] = read_int(&str);
            if(str[0] == '/'){
              str += 1;
              read_int(&str);
              if(str[0] == '/'){
                str += 1;
                f.n[i] = read_int(&str);
              }
            }
          }
          if(face_count >= max_face_count){
            panic("duplicate_obj tried to read in too many faces.");
          }
          faces[face_count++] = f;
        }else if(string_equals(parts[0], "o") && part_count == 2){
          int object_kind = 0;
          int name_index;
          for(name_index = 0; name_index < array_count(object_names); ++name_index){
            if(string_equals(parts[1], object_names[name_index])){
              object_kind = name_index;
              break;
            }
          }
          if(name_index == array_count(object_names)){
            panic("!!! ERROR: .obj file has object '%s', which we don't recognize. SHUTTING DOWN!\n", parts[1]);
          }
          
          if(object_count >= array_count(objects)){
            panic("duplicate_obj tried to read in too many `o XXX` (object) entries.");
          }
          objects[object_count++] = (struct object){
            .kind = object_kind,
            .vertex_begin = vertex_count,
            .normal_begin = normals_count,
            .face_begin = face_count,
          };
        }
      }
    }
    free(line); line = NULL;
    fclose(f);
  }
  
  if(object_count >= array_count(objects)){
    panic("duplicate_obj tried to read in too many `o XXX` (object) entries when writing the last null terminator object.");
  }
  objects[object_count] = (struct object){ // NOTE: null terminator object
    .vertex_begin = vertex_count,
    .normal_begin = normals_count,
    .face_begin = face_count,
  };
  
  // NOTE: Write the new file
  {
    FILE *f = fopen(output_path, "w");
    if(!f){
      panic("duplicate_obj was unable to open the file `%s` for outputting", output_path);
    }
    
    fprintf(f, "# Automatically written by run-jobs.c on "); fprint_current_date(f); fprintf(f, "\n");
    
    int written_vertices_count = 0;
    int written_normals_count = 0;
    
    for(int group_index = 0; group_index < cyclist_count; ++group_index){
      int vertex_base = written_vertices_count;
      int normals_base = written_normals_count;
      
      for(int object_index = 0; object_index < object_count; ++object_index){
        struct object *o = objects + object_index;
        struct object *next_o = o + 1;
        fprintf(f, "g %s%s\n", group_names[group_index], object_names[o->kind]);
        
        for(int vertex_index = o->vertex_begin; vertex_index < next_o->vertex_begin; ++vertex_index){
          fprintf(f, "v %f %f %f\n", vertices[vertex_index].x + (distance + bike_length) * group_index, vertices[vertex_index].y + (y_offset) * group_index, vertices[vertex_index].z - 0.01877/* NOTE(jkron - 2020-01-11): small corrective factor because I am a very bad Blender user... */);
          written_vertices_count += 1;
        }
        
        for(int normals_index = o->normal_begin; normals_index < next_o->normal_begin; ++normals_index){
          fprintf(f, "vn %f %f %f\n", normals[normals_index].x, normals[normals_index].y, normals[normals_index].z);
          written_normals_count += 1;
        }
        
        for(int face_index = o->face_begin; face_index < next_o->face_begin; ++face_index){
          struct face *face = faces + face_index;
          fprintf(f, "f");
          for(int i = 0; i < face->count; ++i){
            fprintf(f, " %d//%d", face->v[i] + vertex_base, face->n[i] + normals_base);
          }
          fprintf(f, "\n");
        }
      }
    }
    
    fclose(f);
  }
  
  free(vertices);
  free(normals);
  free(faces);
}

typedef struct ForceSet ForceSet;
struct ForceSet{
  int time;
  float tx, ty, tz; // Total
  float px, py, pz; // Pressure
  float vx, vy, vz; // Viscous
};
static int compare_force_sets_proc(const void *_a, const void *_b){
  ForceSet *a = (ForceSet*)_a;
  ForceSet *b = (ForceSet*)_b;
  int result = a->time - b->time;
  return result;
}

typedef struct ExtractedForces ExtractedForces;
struct ExtractedForces{
  ForceSet *sets;
  int set_count;
};

static ExtractedForces extract_forces(char *directory){
  ExtractedForces result = {0};
  
  size_t line_size = 8192;
  char *line = malloc(line_size);
  
  int max_force_count = 4096;
  result.sets = calloc(max_force_count, sizeof(ForceSet));
  
  DIR *dir = opendir(directory);
  if(!dir){
    panic("extract_forces was instructed to extract forces from the directory `%s` but was unable to open this directory. This probably indicates that the OpenFOAM simulation did not execute correctly.", directory);
  }
  
  struct dirent *entry;
  while((entry = readdir(dir))){
    if(entry->d_name[0] == '.'){ continue; }
    
    char filename[4096];
    snprintf(filename, sizeof(filename), "%s/%s/force.dat", directory, entry->d_name);
    FILE *f = fopen(filename, "r");
    if(!f){
      panic("extract_forces couldn't open the file `%s` (extracting from the directory `%s`). This is probably a bug in the code of this program and should not happen!", filename, directory);
    }
    while(getline(&line, &line_size, f) != -1){
      if(line[0] != '\0' && line[strlen(line) - 1] == '\n'){ line[strlen(line) - 1] = '\0'; }
      
      if(line[0] != '#' && line[0] != '\0'){
        char *parts[256];
        int part_count = split_on_whitespace(parts, 256, line, false);
        
        if(part_count > 0){
          assert(part_count == 10);
          
          ForceSet *force_set = result.sets + result.set_count++;
          force_set->time = atoi(parts[0]);
          force_set->tx = atof(parts[1] + 1);
          force_set->ty = atof(parts[2]);
          force_set->tz = atof(parts[3]);
          
          force_set->px = atof(parts[4] + 1);
          force_set->py = atof(parts[5]);
          force_set->pz = atof(parts[6]);
          
          force_set->vx = atof(parts[7] + 1);
          force_set->vy = atof(parts[8]);
          force_set->vz = atof(parts[9]);
        }
      }
    }
    fclose(f);
  }
  
  // NOTE: Sort in ascending order according to time.
  qsort(result.sets, result.set_count, sizeof(result.sets[0]), compare_force_sets_proc);
  
  free(line);
  closedir(dir);
  
  return result;
}
static void extract_forces_for_all_cyclists(ExtractedForces *out, char *sim_dir, int cyclist_count){
  fiz(cyclist_count){
    char buf[1024];
    snprintf(buf, sizeof(buf), "%s/postProcessing/forces_%sall/", sim_dir, group_names[i]);
    out[i] = extract_forces(buf);
    
    assert(out[i].set_count > 0);
  }
}

#define FORCE_CONVERGENCE_WINDOW_SIZE 100
static float get_tx_force_convergence(ExtractedForces *forces, int offset){
  int begin = forces->set_count - FORCE_CONVERGENCE_WINDOW_SIZE - offset;
  float result;
  int end = begin + FORCE_CONVERGENCE_WINDOW_SIZE;
  if(0 <= begin && end <= forces->set_count){
    float min_value = INFINITY;
    float max_value = -INFINITY;
    
    for(int i = begin; i < end; ++i){
      float value = forces->sets[i].tx;
      if(value < min_value) min_value = value;
      if(value > max_value) max_value = value;
    }
    result = max_value - min_value;
  }else{
    log_warning("get_tx_force_convergence was requested to operate out of bounds (the interval [%d, %d) is not a subset of [%d, %d)", begin, end, 0, forces->set_count);
    result = 10000;
  }
  return result;
}

static void write_generated_file_header(FILE *f){
  fprintf(f, "/* Written by run-jobs.c ");
  fprint_current_date(f);
  fprintf(f, " */\n");
}

static void print_forces_entry(FILE *f, char *name, char *patch){
  fprintf(f, "forces_%s\n", name);
  fprintf(f, "{\n");
  fprintf(f, "type            forces;\n");
  fprintf(f, "libs            (\"libforces.so\");\n");
  fprintf(f, "patches         (\"%s\");\n", patch);
  fprintf(f, "rho             rhoInf;\n"); // Indicates incompressible
  fprintf(f, "rhoInf          1;\n");      // Required when rho = rhoInf
  fprintf(f, "writeControl    timeStep;\n");
  fprintf(f, "timeInterval    10;\n");
  fprintf(f, "origin          (0 0 0);\n");
  fprintf(f, "e1              (1 0 0);\n");
  fprintf(f, "e2              (0 1 0);\n");
  fprintf(f, "e3              (0 0 1);\n");
  fprintf(f, "log             no;\n");
  fprintf(f, "}\n");
}
static void write_forces_file(char *filename, int cyclist_count){
  FILE *f = fopen(filename, "w");
  if(!f){
    panic("write_forces_file was unable to open the file `%s` for outputting", filename);
  }
  write_generated_file_header(f);
  
  for(int group_index = 0; group_index < cyclist_count; group_index++){
    char name[256], patch[256];
    sprintf(name, "%sall", group_names[group_index]);
    sprintf(patch, "draftingPair_%s.*", group_names[group_index]);
    print_forces_entry(f, name, patch);
    
    for(int object_index = 0; object_index < array_count(object_names); ++object_index){
      sprintf(patch, "draftingPair_%s%s", group_names[group_index], object_names[object_index]);
      sprintf(name, "%s%s", group_names[group_index], object_names[object_index]);
      print_forces_entry(f, name, patch);
    }
  }
  fclose(f);
}

typedef struct ResidualField ResidualField;
struct ResidualField{
  char solver[64];
  double initial[3];
  double final[3];
  int iters[3];
  bool converged;
};

typedef struct ResidualSet ResidualSet;
struct ResidualSet{
  int time;
  ResidualField U;
  ResidualField k;
  ResidualField p;
  ResidualField epsilon;
};
static int compare_residual_sets_proc(const void *_a, const void *_b){
  ResidualSet *a = (ResidualSet*)_a;
  ResidualSet *b = (ResidualSet*)_b;
  int result = a->time - b->time;
  return result;
}

static bool parse_bool_string(char *string){
  bool result;
  if(string_equals(string, "true")){
    result = true;
  }else if(string_equals(string, "false")){
    result = false;
  }else{
    panic("Unable to parse boolean string %s", string);
  }
  return result;
}

static ResidualField read_residual_field(char ***_parts, char **parts_end, int component_count){
  char **parts = *_parts;
  ResidualField result = {0};
  
  copy_string(result.solver, sizeof(result.solver), parts[0]); parts += 1;
  fiz(component_count){
    result.initial[i] = atof(parts[0]); parts += 1;
    result.final[i] = atof(parts[0]); parts += 1;
    result.iters[i] = atoi(parts[0]); parts += 1;
  }
  result.converged = parse_bool_string(parts[0]); parts += 1;
  
  *_parts = parts;
  return result;
}

typedef struct ExtractedResiduals ExtractedResiduals;
struct ExtractedResiduals{
  ResidualSet *sets;
  int set_count;
};
static ExtractedResiduals extract_residuals(char *directory){
  ExtractedResiduals result = {0};
  int max_set_count = 4096;
  result.sets = (ResidualSet*)calloc(max_set_count, sizeof(ResidualSet));
  
  size_t line_size = 8192;
  char *line = malloc(line_size);
  
  DIR *dir = opendir(directory);
  if(!dir){
    panic("extract_residuals was instructed to extract residuals from the directory `%s` but was unable to open it. This indicates that the OpenFOAM simulation did not execute correctly.", directory);
  }
  
  struct dirent *entry;
  while((entry = readdir(dir))){
    if(entry->d_name[0] == '.'){ continue; }
    
    char filename[4096];
    snprintf(filename, sizeof(filename), "%s/%s/solverInfo.dat", directory, entry->d_name);
    FILE *f = fopen(filename, "r");
    if(!f){
      panic("Unable to open file `%s` (for extract_residuals)", filename);
    }
    
    while(getline(&line, &line_size, f) != -1){
      if(line[0] != '\0' && line[strlen(line) - 1] == '\n'){ line[strlen(line) - 1] = '\0'; }
      
      if(line[0] != '#' && line[0] != '\0'){
        char *parts[256];
        int part_count = split_on_whitespace(parts, 256, line, false);
        
        if(part_count > 0){
          assert(part_count == 27);
          ResidualSet set = {0};
          // Time
          // U_solver Ux_initial Ux_final Ux_iters Uy_initial Uy_final Uy_iters Uz_initial Uz_final Uz_iters U_converged
          // k_solver k_initial k_final k_iters k_converged
          // p_solver p_initial p_final p_iters p_converged
          // epsilon_solver epsilon_initial epsilon_final epsilon_iters epsilon_converged
          set.time = atof(parts[0]);
          
          char **parts_cursor = parts + 1;
          char **parts_end = parts + part_count;
          set.U = read_residual_field(&parts_cursor, parts_end, 3);
          set.k = read_residual_field(&parts_cursor, parts_end, 1);
          set.p = read_residual_field(&parts_cursor, parts_end, 1);
          set.epsilon = read_residual_field(&parts_cursor, parts_end, 1);
          
          result.sets[result.set_count++] = set;
        }
      }
    }
    
    fclose(f);
  }
  // NOTE: Sort in ascending order according to time.
  qsort(result.sets, result.set_count, sizeof(result.sets[0]), compare_residual_sets_proc);
  
  free(line);
  closedir(dir);
  
  return result;
}

__attribute__ ((format (printf, 1, 2)))
static int run_command(char *format_command, ...){
  va_list list;
  va_start(list, format_command);
  char buf[8192];
  vsnprintf(buf, sizeof(buf), format_command, list);
  log_info("Running command `%s`...", buf);
  int status = system(buf);
  va_end(list);
  int result;
  if(WIFEXITED(status)){
    result = WEXITSTATUS(status);
    if(result == 0){
      log_info("... command `%s` exited with status %d!", buf, result);
    }else{
      log_warning("... command `%s` exited with nonzero status %d!", buf, result);
    }
  }else{
    result = 0xffffffff;
    log_warning("... command `%s` did not exit cleanly!", buf);
  }
  return result;
}

typedef struct FileContent FileContent;
struct FileContent{
  char *data;
  int64_t size;
};

static FileContent read_entire_text_file(char *path){
  FileContent result = {0};
  
  FILE *file = fopen(path, "r");
  if(file){
    fseek(file, 0, SEEK_END);
    result.size = ftell(file);
    fseek(file, 0, SEEK_SET);
    
    result.data = malloc(result.size + 1);
    if(!result.data){
      panic("Unable to allocate memory for reading in the entirety of the file `%s` (size = %ld bytes)", path, result.size);
    }
    size_t fread_status = fread(result.data, 1, result.size, file);
    if(fread_status != result.size){
      panic("Unable to read the entirety of the file `%s`, could only read %llu bytes out of %llu bytes", path, fread_status, result.size);
    }
    result.data[result.size] = '\0';
    fclose(file);
  }else{
    log_warning("read_entire_text_file was requested to read the file `%s` but was unable to open it for reading", path);
  }
  return result;
}

static bool scan_for_substring(char *string, char *substring){
  size_t substring_len = strlen(substring);
  size_t string_len = strlen(string);
  bool result = false;
  for(char *cursor = string; cursor < string + string_len; ++cursor){
    if(strncmp(cursor, substring, substring_len) == 0){
      result = true;
      break;
    }
  }
  return result;
}

// NOTE(jkron - 2020-01-28): Call this before doing any IO between docker container and host. All it does is wait an amount of time that should be enough for docker to fully syncrhonise
// IO between host and the docker container.
static void let_docker_sync_its_io(){
  write_work_shift_entry("begin_let-docker-sync", "");
  // NOTE(jkron - 2020-02-20): This seems to be long enough (I would guess that this syncing is
  // completly unneccesary now that we worked around the bug in docker found encountered on the
  // computer nimitz).
  int duration = 2;
  log_info("Sleeping for %d seconds in order to let docker sync its io...", duration);
  sleep(duration);
  write_work_shift_entry("end_let-docker-sync", "");
}


typedef enum FoamError FoamError;
enum FoamError{
  FOAM_ERROR_none,
  FOAM_ERROR_generic,
  // NOTE(jkron): Problem where snappyHexMesh seems to randomly crash on some inputs (maybe because a point is exactly on the surface between two cells?).
  FOAM_ERROR_snappy_randomly_failed,
  FOAM_ERROR_missing_log_file,
  FOAM_ERROR_MAX
};
static char *foam_error_as_string[FOAM_ERROR_MAX] = {
  "none (if you are seing this string something is probably amiss)",
  "generic",
  "snappy randomly failed (the \"Point (XXX XXX XXX) is not inside the mesh or on a face or edge\" error)",
  "missing log file (yes, we extract crash status from the log files...)",
};

static FoamError get_crash_status_from_log_file(char *log_path){
  FoamError result = 0;
  if(!file_exists(log_path)){
    log_warning("get_crash_status_from_log_file was instructed to read the file `%s` but that file does not even exist!", log_path);
    result = FOAM_ERROR_missing_log_file;
  }else{
    FileContent log_file = read_entire_text_file(log_path);
    
    if(!log_file.data) panic("get_crash_status_from_log_file was requested to read `%s` was not able to, even though the file exists (according to the file_exists()-procedure) :-(", log_path);
    
    
    bool fatal_error_found = scan_for_substring(log_file.data, "FATAL ERROR");
    
    if(scan_for_substring(log_file.data, "is not inside the mesh or on a face or edge")){
      result = FOAM_ERROR_snappy_randomly_failed;
      
      if(!fatal_error_found){
        log_warning("Found the `Point (XXX XXX XXX) is not inside the mesh or on a face or edge.` pattern but no FOAM FATAL ERROR. This indicates that something is weird (probably the error detection code)");
      }
    }else if(fatal_error_found || scan_for_substring(log_file.data, "MPI ABORT")){
      result = FOAM_ERROR_generic;
    }
    
    free(log_file.data);
  }
  return result;
}
typedef struct DockerCommandResult DockerCommandResult;
struct DockerCommandResult{
  bool success;
  FoamError foam_error; // Resulting foam error (extracted from log file), if any.
};
static DockerCommandResult run_foam_tool_in_docker(time_t max_duration, char *foam_tool){
  DockerCommandResult result = {0};
  time_t initial_time = time(NULL);
  write_work_shift_entry("begin_run-foam-tool-in-docker", foam_tool);
  
  if(run_command("docker container restart of_v1906") != 0){
    panic("Unable to (re)start docker container!\n");
  }
  
  log_info("Running foam tool %s (maximum duration is set to %d seconds)...", foam_tool, (int)max_duration);
  
  char working_dir[4096];
  snprintf(working_dir, sizeof(working_dir), "%s/active-sim/", auto_directory_path);
  
  char log_path[4096];
  snprintf(log_path, sizeof(log_path), "%s/log.%s", working_dir, foam_tool);
  
  pid_t pid = fork();
  if(pid == 0){
    let_docker_sync_its_io();
    
    int input_fd = open("/dev/null", O_RDONLY);
    if(input_fd == -1){
      panic("open(\"/dev/null\") failed");
    }
    int output_fd = STDERR_FILENO;
    
    // Child
    int result0 = dup2(input_fd, STDIN_FILENO);
    int result1 = dup2(output_fd, STDOUT_FILENO);
    int result2 = dup2(output_fd, STDERR_FILENO);
    if(result0 == -1 || result1 == -1 || result2 == -1){
      panic("dup2 failes (when trying to fork in order to run docker)");
    }
    close(input_fd);
    close(output_fd);
    
    char *arguments[64];
    int argument_count = 0;
    arguments[argument_count++] = "/usr/bin/docker";
    arguments[argument_count++] = "exec";
    arguments[argument_count++] = "-t";
    arguments[argument_count++] = "-w";
    arguments[argument_count++] = working_dir;
    arguments[argument_count++] = "of_v1906";
    arguments[argument_count++] = "/bin/bash";
    arguments[argument_count++] = "-c";
    char bash_command[4096];
    snprintf(bash_command, sizeof(bash_command), "./script_%s.sh", foam_tool);
    arguments[argument_count++] = bash_command;
    arguments[argument_count++] = NULL;
    
    execv("/usr/bin/docker", arguments);
    int errno_value = errno;
    
    fprintf(stderr, "execv failed: errno = %d (%s)\n", errno_value, strerror(errno_value));
    fflush(stderr);
#undef exit
    exit(1);
#define exit DONT USE THIS THING USE PANIC INSTEAD UNLESS YOU HAVE FORKED IN WHICH CASE YOU DONT WANNA DO THAT
  }
  
  // Parent
  bool child_running = true;
  uint64_t counter = 0;
  
  while(child_running){
    int wstatus;
    sleep(10);
    int waitpid_status = waitpid(pid, &wstatus, WNOHANG);
    if(waitpid_status == pid){
      child_running = false; 
      
      if(WIFEXITED(wstatus)){
        int exit_status = WEXITSTATUS(wstatus);
        if(exit_status == 0){
          result.success = true;
        }
      }
    }
    
    if(child_running){
      bool overtime = time(NULL) - initial_time > max_duration;
      counter += 1;
      if(counter % 100 == 0){
        // NOTE(jkron - 2020-01-28): This is not checked particularly often for performance reasons.
        result.foam_error = get_crash_status_from_log_file(log_path);
      }
      
      if(overtime || ctrl_c_detected || result.foam_error){
        child_running = false;
        if(!result.foam_error){
          let_docker_sync_its_io();
          result.foam_error = get_crash_status_from_log_file(log_path);
        }
        
        if(result.foam_error){
          log_warning("It has been detected that the foam tool `%s` has failed (error is `%s`).", foam_tool, foam_error_as_string[result.foam_error]);
        }else if(overtime){
          log_warning("Foam tool `%s` has been running for longer than it's maximum duration (%d seconds) and will be forcefully terminated!", foam_tool, (int)max_duration);
        }else if(ctrl_c_detected){
          ctrl_c_detected = false;
          log_warning("Foam tool `%s` will now be terminated upon request (a SIGINT was recieved AKA CTRL+C was pressed)", foam_tool);
        }else{
          assert(false);
        }
        
        int kill_status = kill(pid, 9/*SIGKILL*/);
        int errno_value = errno;
        if(kill_status == -1){
          if(errno_value == ESRCH){
            // NOTE(jkron - 220-01-16): This means that the process with pid 'pid' does not exist, which would be weird but maybe possible due to some odd thing I don't know about linux (or just due to the race condition present (in the overtime case) but that seems unlikely).
            log_warning("kill(pid, ...) tells us that the process doesn't exist but waitpid hasn't told us that the process has exited... weird.");
          }else{
            // NOTE(jkron - 2020-01-16): Here EPERM and EINVAL would be very weird.
            panic("Killing the process running in docker failed: errno is %d (%s)", errno_value, strerror(errno_value));
          }
        }
      }
    }
  }
  
  {
    // @HACK(jkron - 2020-01-21): Because 'docker exec XXX' can exit before the process running
    // in docker exits (although it seems like this is unusual) we must have a way of killing
    // the process in docker lest that process will remain and use too much RAM for other processes
    // to run. Thus a file called 'inside_docker.pid' is created from within docker so that we can
    // properly kill that process.
    
    char path[4096];
    snprintf(path, sizeof(path), "%s/inside_docker.pid", working_dir);
    FILE *pid_file = fopen(path, "r");
    if(pid_file){
      log_warning("After running the foam tool `%s` within docker (probably non-successfully) the file 'inside_docker.pid' was left behind. As it might be the case that the process with the pid indicated by the file is still running we will now try to kill it and then remove the file containing the PID.", foam_tool);
      int pid_in_docker;
      fscanf(pid_file, "%d", &pid_in_docker);
      
      run_command("docker exec of_v1906 /bin/sh -c \"kill %d\"", pid_in_docker);
      
      fclose(pid_file);
      unlink(path);
    }
  }
  
  // NOTE(jkron - 2020-02-13): Stop the container so that docker will (hopefully) sync its files. This is something that we are having problems with on one computer ("nimitz").
  if(run_command("docker container stop of_v1906") != 0){
    panic("Unable to stop docker container!\n");
  }
  
  if(!result.foam_error){
    int attempt_count = 0;
    int max_attempt_count = 10;
    
    do{
      let_docker_sync_its_io();
      result.foam_error = get_crash_status_from_log_file(log_path);
      if(result.foam_error == FOAM_ERROR_missing_log_file){
        log_warning("Unable to open the log file `%s`, which is either due to it actually not existing or docker has not synced the log file properly yet. Due to the possibility that this is the latter case  we will retry a couple of times (this is attempt %d out of %d maximum attempts) with a delay inbetween.n", log_path, attempt_count, max_attempt_count);
        let_docker_sync_its_io();
        attempt_count += 1;
      }
    }while(result.foam_error == FOAM_ERROR_missing_log_file && attempt_count < max_attempt_count);
    
    if(result.foam_error){
      result.success = false;
      log_warning("Found crash status in log file `%s` which we identified as the error \"%s\"", log_path, foam_error_as_string[result.foam_error]);
    }
  }
  
  write_work_shift_entry("end_run-foam-tool-in-docker", foam_tool);
  
  return result;
}

typedef struct Step Step;
struct Step{
  int duration;
  int stage;
};

typedef struct JobConfiguration JobConfiguration;
struct JobConfiguration{
  char name[256];
  char path[256];
  FILE *out; // NOTE(jkron - 2020-01-13): For writing to the job file as a log file (remember that it must remain parsable!)
  
  int version;
  float distance;
  float y_offset; // @V3
  float bike_speed;
  
  IterSpec iter_specs[3]; // @V7
  int maximum_simpleFoam_iterations; // @V7
  
#define MAX_CYCLIST_COUNT 2
  int cyclist_count; // @V3
  // int iteration_count; // @V4 removed in @V5
  
  // float required_convergence[3]; // Required convergence for each of the stages @V6 removed in @V7
  
  // Optionally overwritten. Meant for jobs where the default makes it diverge.
  bool override_initial;
  double k0;
  double epsilon0;
  
  // Automatically derived, the intended to be shown to the user.
  double reynolds_number; // Re
  double turbulence_intensity; // I_t
  
  // NOTE: The following results from running the simulation
  int total_runtime;
  int mesh_runtime;
  int sim_runtime;
  
  int box_max_extra[3]; // @V7
  int box_min_extra[3]; // @V7
  
  Step steps[64];
  int step_count;
};
static JobConfiguration read_configuration(char *directory_path, char *job_name){
  JobConfiguration result = {0};
  copy_string(result.name, sizeof(result.name), job_name);
  snprintf(result.path, sizeof(result.path), "%s/%s", directory_path, job_name);
  
  FILE *f = fopen(result.path, "r");
  if(!f){
    panic("read_configuration was told to use the job file `%s` but was unable to open it for reading!", result.path);
  }
  
  result.out = fopen(result.path, "a");
  if(!result.out){
    panic("read_configuration was told to use the job file `%s` but was unable to open it for writing!", result.path);
  }
  
  size_t line_size = 8192;
  char *line = malloc(line_size);
  while(getline(&line, &line_size, f) >= 0){
    char *parts[256] = {0};
    split_on_whitespace(parts, array_count(parts), line, false);
    
    if(string_equals(parts[0], "version")){
      result.version = atoi(parts[1]);
    }else if(string_equals(parts[0], "distance")){
      result.distance = atof(parts[1]);
    }else if(string_equals(parts[0], "y-offset")){
      result.y_offset = atof(parts[1]);
    }else if(string_equals(parts[0], "bike-speed")){
      result.bike_speed = atof(parts[1]);
    }else if(string_equals(parts[0], "initial-override")){
      result.override_initial = true;
      
      if(!string_equals(parts[1], "epsilon0")){
        panic("The syntax for initial-override is `initial-override epsilon0 <value> k0 <value>` but epsilon0 is missing (or not in the right place) in `%s`", result.path);
      }
      if(!string_equals(parts[3], "k0")){
        panic("The syntax for initial-override is `initial-override epsilon0 <value> k0 <value>` but k0 is missing (or not in the right place) in `%s`", result.path);
      }
      
      result.epsilon0 = atof(parts[2]);
      result.k0 = atof(parts[4]);
    }else if(string_equals(parts[0], "cyclist-count")){
      result.cyclist_count = atoi(parts[1]);
    }else if(string_equals(parts[0], "total-runtime")){
      result.total_runtime = atoi(parts[1]);
    }else if(string_equals(parts[0], "mesh-runtime")){
      result.mesh_runtime = atoi(parts[1]);
    }else if(string_equals(parts[0], "sim-runtime")){
      result.sim_runtime = atoi(parts[1]);
    }else if(string_equals(parts[0], "running-sim")){
      int index = 1;
      
      int from = -1;
      int to = -1;
      int stage = -1;
      
      while(parts[index + 1]){
        if(string_equals(parts[index], "from")){
          from = atoi(parts[index + 1]);
          index += 2;
        }else if(string_equals(parts[index], "to")){
          to = atoi(parts[index + 1]);
          index += 2;
        }else if(string_equals(parts[index], "stage")){
          stage = atoi(parts[index + 1]);
          index += 2;
        }
      }
      
      assert(from >= 0);
      assert(to >= 0);
      assert(stage >= 0);
      result.steps[result.step_count].duration = to - from;
      result.steps[result.step_count].stage = stage;
      result.step_count += 1;
    }else if(string_equals(parts[0], "step-iter-counts")){
      int part_index = 1;
      for(int i = 0; i < 3; ++i){
        if(string_equals(parts[part_index], "fixed")){
          result.iter_specs[i].kind = ITER_SPEC_fixed;
          result.iter_specs[i].fixed = atoi(parts[part_index + 1]);
          part_index += 2;
        }else if(string_equals(parts[part_index], "converge")){
          result.iter_specs[i].kind = ITER_SPEC_converge;
          result.iter_specs[i].converge = atof(parts[part_index + 1]);
          part_index += 2;
        }else{
          panic("While parsing a \"step-iter-counts\" entry in job file %s we got an unexpected stage ending condition kind (we expected \"fixed\" or \"converge\")");
        }
      }
    }else if(string_equals(parts[0], "maximum-simpleFoam-iterations")){
      result.maximum_simpleFoam_iterations = atoi(parts[1]);
    }else if(string_equals(parts[0], "box-min-extra")){
      result.box_min_extra[0] = atoi(parts[1]);
      result.box_min_extra[1] = atoi(parts[2]);
      result.box_min_extra[2] = atoi(parts[3]);
    }else if(string_equals(parts[0], "box-max-extra")){
      result.box_max_extra[0] = atoi(parts[1]);
      result.box_max_extra[1] = atoi(parts[2]);
      result.box_max_extra[2] = atoi(parts[3]);
    }
  }
  fclose(f);
  free(line);
  
  double freestream = result.bike_speed;
  double A = 0.651937904; // Area as of 2019-12-10
  double L = sqrt(A);
  double viscosity = 1.516e-5; // Stod iallafall i rapporten 2019-12-11
  double density = 1;
  result.reynolds_number = freestream * L * density / viscosity;
  result.turbulence_intensity = 0.16 * pow(result.reynolds_number, -1.0/8);
  
  if(!result.override_initial){
    result.k0 = 3.0 / 2.0 * square(freestream * result.turbulence_intensity); // Från overleaf-dokumentet (som i sin tur hämtat denna kunskap från CFD-online-wikit)
    result.epsilon0 = pow(0.09, 0.75) * pow(result.k0, 3.0/2.0) / L; // NOTE(jkron - 2020-01-11): CFD-online wiki says that we should only multiply by 0.09 while the OpenFOAM documentation (https://www.openfoam.com/documentation/guides/latest/doc/guide-turbulence-ras-k-epsilon.html) says that we should multiply by 0.09^0.75. I made this change right before trying different bike speeds (because forcing a value of 0.3 wouldn't do for varying speeds) and the previously used formula (the one from the CFD online wiki) almost always seemed to result in diverging simulations.
  }
  
  return result;
}
static void free_job_conf(JobConfiguration *conf){
  fclose(conf->out);
  *conf = (JobConfiguration){0};
}

/* finds the directory entry consisting only of base 10 digits whose numerical value is the greatest */
static int find_greatest_number_in_directory(char *directory){
  DIR *dir = opendir(directory);
  if(!dir){
    panic("find_greatest_number_in_directory was requested to use the directory `%s` but was unable to open that directory", directory);
  }
  
  int result = 0;
  
  struct dirent *ent;
  while((ent = readdir(dir))){
    char *name = ent->d_name;
    
    bool is_number_dir = true;
    fiz(strlen(name)){
      if(name[i] < '0' || '9' < name[i]){
        is_number_dir = false;
      }
    }
    
    if(is_number_dir){
      int value = read_int(&name);
      if(value > result) result = value;
    }
  }
  
  closedir(dir);
  return result;
}

typedef struct JobStep1Result JobStep1Result;
struct JobStep1Result{
  char failure_message[512];
  bool success;
};

// NOTE(jkron - 2020-02-05): According to Olander (2011) you can add extra space behind what you simulate to make steady-state solutions more stable.
#define BOX_EXTRA_X (10)

static void config_box_size_for_active_sim(int box_min[3], int box_max[3], float y_offset){
  FILE *f = fopen("active-sim/system/blockMeshDict", "w");
  if(!f){ panic("unable to fopen the blockMeshDict file for writing!"); }
  write_generated_file_header(f);
  char *header = "FoamFile\n"
    "{\n"
    "version     2.0;\n"
    "format      ascii;\n"
    "class       dictionary;\n"
    "object      blockMeshDict;\n"
    "}\n";
  fprintf(f, "%s", header);
  
  fprintf(f, "scale   1;\n" "\n");
  
  int box_cell_count[3] = { (box_max[0] - box_min[0]) * 3, (box_max[1] - box_min[1]) * 3, (box_max[2] - box_min[2]) * 3 };
  
  fprintf(f, "vertices\n" "(\n");
  fprintf(f, "(%d %d %d)\n", box_min[0], box_min[1], box_min[2]);
  fprintf(f, "(%d %d %d)\n", box_max[0], box_min[1], box_min[2]);
  fprintf(f, "(%d %d %d)\n", box_max[0], box_max[1], box_min[2]);
  fprintf(f, "(%d %d %d)\n", box_min[0], box_max[1], box_min[2]);
  fprintf(f, "(%d %d %d)\n", box_min[0], box_min[1], box_max[2]);
  fprintf(f, "(%d %d %d)\n", box_max[0], box_min[1], box_max[2]);
  fprintf(f, "(%d %d %d)\n", box_max[0], box_max[1], box_max[2]);
  fprintf(f, "(%d %d %d)\n", box_min[0], box_max[1], box_max[2]);
  fprintf(f, ");\n");
  
  fprintf(f,
          "\n"
          "blocks\n"
          "(\n"
          "hex (0 1 2 3 4 5 6 7) (%d %d %d) simpleGrading (1 1 1)\n"
          ");\n"
          "\n", box_cell_count[0], box_cell_count[1], box_cell_count[2]);
  
  fprintf(f, "%s",
          "edges\n"
          "(\n"
          ");\n"
          "\n"
          "boundary\n"
          "(\n"
          "frontAndBack\n"
          "{\n"
          "type patch;\n"
          "faces\n"
          "(\n"
          "(3 7 6 2)\n"
          "(1 5 4 0)\n"
          ");\n"
          "}\n"
          "inlet\n"
          "{\n"
          "type patch;\n"
          "faces\n"
          "(\n"
          "(0 4 7 3)\n"
          ");\n"
          "}\n"
          "outlet\n"
          "{\n"
          "type patch;\n"
          "faces\n"
          "(\n"
          "(2 6 5 1)\n"
          ");\n"
          "}\n"
          "lowerWall\n"
          "{\n"
          "type wall;\n"
          "faces\n"
          "(\n"
          "(0 3 2 1)\n"
          ");\n"
          "}\n"
          "upperWall\n"
          "{\n"
          "type patch;\n"
          "faces\n"
          "(\n"
          "(4 5 6 7)\n"
          ");\n"
          "}\n"
          ");\n");
  
  fclose(f); f = NULL;
  
  //
  // Generate custom refinement box for snappyHexMesh
  //
  
  char *refinementBoxes_path = "active-sim/system/snappyHexMeshDict_refinementBoxes";
  f = fopen(refinementBoxes_path, "w");
  if(!f){
    panic("config_box_size_for_active_sim was unable to open the file `%s` for writing", refinementBoxes_path);
  }
  write_generated_file_header(f);
  fprintf(f, "refinementBox\n");
  fprintf(f, "{\n");
  fprintf(f, "type searchableBox;\n");
  
  float y_min = -1.4 + minimum(y_offset, 0);
  float y_max = 1.4 + maximum(y_offset, 0);
  
  fprintf(f, "min  (%d %f 0.0);\n", box_min[0] + 2, y_min);
  fprintf(f, "max  (%d %f 2.5);\n", box_max[0] - 2 - BOX_EXTRA_X, y_max);
  fprintf(f, "}\n");
  fclose(f); f = NULL;
}

typedef struct RectangularSample RectangularSample;
struct RectangularSample{
  float *p;
  float *Ux;
  float *Uy;
  float *Uz;
  int width;
  int height;
};

static RectangularSample extract_rectangular_sample(char *lines_dir, int width, int height){
  RectangularSample result = {0};
  
  result.width = width;
  result.height = height;
  result.p =  calloc(result.width * result.height, sizeof(float));
  result.Ux = calloc(result.width * result.height, sizeof(float));
  result.Uy = calloc(result.width * result.height, sizeof(float));
  result.Uz = calloc(result.width * result.height, sizeof(float));
  
  int num = find_greatest_number_in_directory(lines_dir);
  
  char directory[4096];
  snprintf(directory, sizeof(directory), "%s/%d/", lines_dir, num);
  
  size_t line_size = 8192;
  char *line = malloc(line_size);
  
  for(int scanline = 0; scanline < height; ++scanline){
    // NOTE(jkron - 2020-02-18): The file format here is that each line first contains the Y-coordinate of
    // the sample and the rest of the line is the sampled value. The problem here
    // is that OpenFOAM can sometimes duplicate samples for some unknown reason
    // (and as far as we know it does not guarantee any ordering on the lines) so
    // we map the y coordinate into an array index instead. Also, we are not entirely
    // sure about the wierd behaviour sometimes encountered when entries are missing,
    // so maybe this will produce more correct behaviour there as well.
    
    {
      char U_path[4096];
      snprintf(U_path, sizeof(U_path), "%s/scanline%d_U.xy", directory, scanline);
      
      FILE *U_file = fopen(U_path, "r");
      if(!U_file){
        panic("extract_rectangular_sample was unable to open the file `%s` for reading", U_path);
      }
      while(getline(&line, &line_size, U_file) >= 0){
        char *parts[256] = {0};
        int part_count = split_on_whitespace(parts, array_count(parts), line, false);
        assert(part_count == 4);
        
        float y = atof(parts[0]);
        int array_x_index = (y + 1) / 2.0f * result.width - 0.00001f;
        if(array_x_index < 0 || array_x_index >= result.width){
          log_warning("OpenFOAM lines sample is out of bounds. Ignoring it!");
        }else{
          result.Ux[scanline * width + array_x_index] = atof(parts[1]);
          result.Uy[scanline * width + array_x_index] = atof(parts[2]);
          result.Uz[scanline * width + array_x_index] = atof(parts[3]);
        }
      }
      fclose(U_file); U_file = 0;
    }
    
    {
      char p_path[4096];
      snprintf(p_path, sizeof(p_path), "%s/scanline%d_p.xy", directory, scanline);
      
      FILE *p_file = fopen(p_path, "r");
      if(!p_file){
        panic("extract_rectangular_sample was unable to open the file `%s` for reading", p_path);
      }
      while(getline(&line, &line_size, p_file) >= 0){
        char *parts[256] = {0};
        int part_count = split_on_whitespace(parts, array_count(parts), line, false);
        assert(part_count == 2);
        
        float y = atof(parts[0]);
        
        int array_x_index = (y + 1) / 2.0f * result.width - 0.00001f;
        if(array_x_index < 0 || array_x_index >= result.width){
          log_warning("OpenFOAM lines sample is out of bounds. Ignoring it!");
        }else{
          result.p[scanline * width + array_x_index] = atof(parts[1]);
        }
      }
      fclose(p_file); p_file = 0;
    }
  }
  free(line);
  
  return result;
}
static void free_rectangular_sample_data(RectangularSample sample){
  free(sample.p);
  free(sample.Ux);
  free(sample.Uy);
  free(sample.Uz);
}

#define RECTANGULAR_SAMPLE_DIM_X 200
#define RECTANGULAR_SAMPLE_DIM_Y 200


static void copy_file(char *source, char *destination){
  run_command("cp \"%s\" \"%s\"", source, destination);
}


static JobStep1Result job_step1_prepare_and_run_simulation(JobConfiguration *conf){
  log_info("Running step 1: Prepare the simulation, generate the mesh and run it (simulation = %s)", conf->name);
  
  JobStep1Result result = {0};
  
  time_t job_start_time = time(NULL);
  
  //
  // NOTE: Prepare the OpenFOAM-files for the simulation
  //
  
  run_command("cp -r sim-template-k-epsilon active-sim");
  
  int current_job_version_number = 8;
  if(conf->version != current_job_version_number){
    panic("job version number in job file %s is %d which is not supported. We are currently at version %d.\n", conf->name, conf->version, current_job_version_number);
  }
  
  duplicate_obj("one_cyclist_v3.obj", "active-sim/constant/triSurface/draftingPair.obj", conf->distance, conf->y_offset, conf->cyclist_count);
  run_command("gzip active-sim/constant/triSurface/draftingPair.obj");
  
  {
    FILE *f = fopen("active-sim/0.orig/include/initialConditions", "w");
    if(!f){ panic("unable to fopen the initialConditions file for writing!"); }
    write_generated_file_header(f);
    double freestream = conf->bike_speed;
    fprintf(f, "flowVelocity     (%f 0 0);\n", freestream);
    fprintf(f, "pressure         0;\n");
    fprintf(f, "turbulentKE      %f;\n", conf->k0);
    fprintf(f, "turbulentEpsilon %f;\n", conf->epsilon0);
    fclose(f);
  }
  
  int box_min[3] = {  -5, -8, 0 };
  
  if(conf->cyclist_count > 2){ panic("More than two cyclists not supported!"); }
  int box_max[3] = {  conf->distance + 8 + BOX_EXTRA_X,  8, 8 };
  fiz(3){
    box_min[i] -= conf->box_min_extra[i];
    box_max[i] += conf->box_max_extra[i];
  }
  config_box_size_for_active_sim(box_min, box_max, conf->y_offset);
  
  write_forces_file("active-sim/system/forces", conf->cyclist_count);
  
  { // Write lines file
    /*
NOTE(jkron - 2020-02-13):

This stuff was intended for being able to automatically and more quickly analyze data
 than when sampled cutting planes are used, but it turns out that OpenFOAM does not like
 lots of sample points because the following problems are observed:
 
 1) Some sampled points are simply rejected for whatever reason.
 2) Weird effects can be seen in the generated images that indicates that the sampled 
 points are not sampled as nicely as we had hoped for.
 
 Because of these things cutting planes are used instead, even though they are very
 annoying to work with.
*/
    
    char *lines_path = "active-sim/system/lines";
    FILE *f = fopen(lines_path, "w");
    if(!f){
      panic("Unable to open file `%s` for writing", lines_path);
    }
    write_generated_file_header(f);
    
    float w = 2;
    float h = 2;
    
    float x_offset = 0.1;
    float x_coord = bike_length + conf->distance - x_offset;
    
    fprintf(f, "in_front_of_back_cyclist\n");
    fprintf(f, "{\n");
    fprintf(f, "type                 sets;\n");
    fprintf(f, "libs                 (\"libsampling.so\");\n");
    fprintf(f, "outputControl        outputTime;\n");
    fprintf(f, "interpolationScheme  cellPoint;\n");
    fprintf(f, "setFormat            raw;\n");
    fprintf(f, "fields               (U p);\n");
    fprintf(f, "sets\n");
    fprintf(f, "(\n");
    for(int i = 0; i < RECTANGULAR_SAMPLE_DIM_Y; ++i){
      fprintf(f, "scanline%d\n", i);
      fprintf(f, "{\n");
      fprintf(f, "type        uniform;\n");
      fprintf(f, "start       (%f %f %f);\n", x_coord, -w * 0.5f, i * h / (RECTANGULAR_SAMPLE_DIM_Y - 1));
      fprintf(f, "end         (%f %f %f);\n", x_coord,  w * 0.5f, i * h / (RECTANGULAR_SAMPLE_DIM_Y - 1));
      fprintf(f, "axis        y;\n"); // NOTE: This value is printed in the output (no idea why its only a single value though...)
      fprintf(f, "nPoints     %d;\n", RECTANGULAR_SAMPLE_DIM_X);
      fprintf(f, "}\n");
    }
    
    fprintf(f, ");\n");
    fprintf(f, "}\n");
    fprintf(f, "\n");
    
    fclose(f);
  }
  
  {
    // Write cuttingPlanes_generated file
    FILE *file = fopen("active-sim/system/cuttingPlanes_generated", "w");
    write_generated_file_header(file);
    
    fprintf(file, "sideview_back\n");
    fprintf(file, "{\n");
    fprintf(file, "type            cuttingPlane;\n");
    fprintf(file, "planeType       pointAndNormal;\n");
    fprintf(file, "pointAndNormalDict\n");
    fprintf(file, "{\n");
    fprintf(file, "point   (0 %f 0);\n", conf->y_offset);
    fprintf(file, "normal  (0 1 0);\n");
    fprintf(file, "}\n");
    fprintf(file, "interpolate     true;\n");
    fprintf(file, "}\n");
    
    fprintf(file, "in_front_of_back\n");
    fprintf(file, "{\n");
    fprintf(file, "type            cuttingPlane;\n");
    fprintf(file, "planeType       pointAndNormal;\n");
    fprintf(file, "pointAndNormalDict\n");
    fprintf(file, "{\n");
    fprintf(file, "point   (%f 0 0);\n", conf->distance + bike_length - 0.1f);
    fprintf(file, "normal  (1 0 0);\n");
    fprintf(file, "}\n");
    fprintf(file, "interpolate     true;\n");
    fprintf(file, "}\n");
    
    fclose(file);
  }
  
  //
  // Run the simulation (meshing + actual simulation)
  //
  
  time_t meshing_start_time = time(NULL);
  
  DockerCommandResult mesh_result = {0};
  {
    int mesh_num_tries = 0;
    for(;; ++mesh_num_tries){
      {fprintf(conf->out, "gen-mesh ");
        fprint_current_date(conf->out);
        fprintf(conf->out, "\n");
        fflush(conf->out);}
      
      mesh_result = run_foam_tool_in_docker(500, "blockMesh");
      if(!mesh_result.success){
        log_warning("blockMesh failed!");
        snprintf(result.failure_message, sizeof(result.failure_message), "mesh-generation-blockMesh-failed");
      }
      
      if(mesh_result.success){
        mesh_result = run_foam_tool_in_docker(500, "decomposePar");
        if(!mesh_result.success){
          log_warning("decomposePar failed!");
          snprintf(result.failure_message, sizeof(result.failure_message), "mesh-generation-decomposePar-failed");
        }
      }
      if(mesh_result.success){
        mesh_result = run_foam_tool_in_docker(2000, "snappyHexMesh");
        
        if(!mesh_result.success){
          if(mesh_result.foam_error == FOAM_ERROR_snappy_randomly_failed){
            // HACK(jkron - 2020-01-28): It seems like this problem can be fixed
            // by slightly nudging the blockMesh dimension. We will try to do that
            // a number of times before giving up with this simulation.
            
            // NOTE(jkron - 2020-02-21): We finally found out that this problem in snappyHexMesh is caused by
            // the point specified by the `locationInsideMesh` in snappyHexMeshDict being in the border between two cells.
            // By setting this point to something which can impossibly lie on a border the problem can be completly avoided.
            if(mesh_num_tries >= 5){
              snprintf(result.failure_message, sizeof(result.failure_message), "mesh-generation-retried-maximum-number-of-times");
              break;
            }else{
              log_warning("We hit the weird crash in snappyHexMesh and will retry with a slightly different size for blockMesh");
              box_max[0] += 2;
              config_box_size_for_active_sim(box_min, box_max, conf->y_offset);
              
              run_command("cd active-sim/ && ./Allclean");
            }
          }else{
            log_warning("snappyHexMesh crashed (but it was not the weird point out of bounds thing that we can recover from)");
            snprintf(result.failure_message, sizeof(result.failure_message), "mesh-generation-snappyHexMesh-failed");
            break;
          }
        }else{
          break;
        }
      }else{
        break;
      }
    }
  }
  
  if(mesh_result.success){
    time_t meshing_end_time = time(NULL);
    conf->mesh_runtime = meshing_end_time - meshing_start_time;
    fprintf(conf->out, "mesh-runtime %d\n", conf->mesh_runtime);
    fflush(conf->out);
    
    log_info("Done generating mesh in %d seconds. Now running simulation!", conf->mesh_runtime);
    time_t simulation_start_time = meshing_end_time;
    
    DockerCommandResult sim_result = run_foam_tool_in_docker(500, "renumberMesh");
    
    copy_file("fvSchemes/fvSchemes_0", "active-sim/system/fvSchemes");
    if(sim_result.success) sim_result = run_foam_tool_in_docker(500, "potentialFoam");
    if(sim_result.success) sim_result = run_foam_tool_in_docker(500, "checkMesh");
    
    if(!sim_result.success){
      log_warning("One of the tools used when running the simulation failed!");
      snprintf(result.failure_message, sizeof(result.failure_message), "simulation-generic");
    }else{
      int simpleFoam_iteration = 0;
      
      // NOTE(jkron - 2020-02-07): The simulations are run with 3 different fvScheme-files.
      int stage = 0;
      int stage_count = array_count(conf->iter_specs);
      
      while(stage < stage_count && simpleFoam_iteration < conf->maximum_simpleFoam_iterations && sim_result.success){
        {
          char fd_schemes_path[4096];
          snprintf(fd_schemes_path, sizeof(fd_schemes_path), "%s/fvSchemes/fvSchemes_%d", auto_directory_path, stage);
          copy_file(fd_schemes_path, "active-sim/system/fvSchemes");
        }
        
        IterSpec iter_spec = conf->iter_specs[stage];
        
        int iterations_to_do;
        if(iter_spec.kind == ITER_SPEC_fixed){
          iterations_to_do = iter_spec.fixed;
        }else if(iter_spec.kind == ITER_SPEC_converge){
          iterations_to_do = 50;
        }else{
          panic("JobConfiguration::iter_specs[%d] has unexpected value %d", stage, conf->iter_specs[stage].kind);
        }
        
        int prev_iteration = simpleFoam_iteration;
        int next_iteration = simpleFoam_iteration + iterations_to_do;
        
        log_info("Running simulation %s from iteration %d to iteration %d (stage = %d)", conf->name, prev_iteration, next_iteration, stage);
        fprintf(conf->out, "running-sim from %d to %d stage %d ", prev_iteration, next_iteration, stage); fprint_current_date(conf->out); fprintf(conf->out, "\n");
        fflush(conf->out);
        
        Step step = { .duration = next_iteration - prev_iteration, .stage = stage };
        conf->steps[conf->step_count++] = step;
        
        {
          char *control_time_path = "active-sim/system/controlDict_time";
          FILE *f = fopen(control_time_path, "w");
          if(!f){
            panic("Was unable to open file `%s` for writing", control_time_path);
          }
          fprintf(f, "/* Written by run-jobs.c "); fprint_current_date(f); fprintf(f, " */\n");
          
          fprintf(f, "startFrom       startTime;\n");
          fprintf(f, "startTime       %d;\n", prev_iteration);
          fprintf(f, "stopAt          endTime;\n");
          fprintf(f, "deltaT          1;\n");
          fprintf(f, "writeControl    timeStep;\n");
          fprintf(f, "endTime         %d;\n", next_iteration);
          fprintf(f, "writeInterval   %d;\n", next_iteration);
          fprintf(f, "\n");
          fclose(f);
        }
        
        run_command("mv active-sim/log.simpleFoam active-sim/log_%d_to_%d.simpleFoam", prev_iteration, next_iteration);
        sim_result = run_foam_tool_in_docker(5000, "simpleFoam");
        simpleFoam_iteration += iterations_to_do;
        
        if(sim_result.success){
          char time_dir_path[256];
          snprintf(time_dir_path, sizeof(time_dir_path), "active-sim/processor0/%d", next_iteration);
          if(!file_exists(time_dir_path)){
            panic("Simulation reported that it was completed successfully but the directory for the time we were supposed to run to (time=%d) does not exist! This indicates that something is very wrong!!!!", next_iteration);
            break;
          }
          
          { // NOTE: For each iteration OpenFOAM will output data for that iteration in the active-sim/processor* directories, which is rather big and completly unneccesary.
            DIR *dir = opendir("active-sim/");
            if(!dir){
              panic("Could not opendir() the active-sim/ directory. Something is wrong!");
            }
            
            for(struct dirent *ent; (ent = readdir(dir));){
              if(strncmp(ent->d_name, "processor", 9) == 0){
                char to_remove[4096];
                snprintf(to_remove, sizeof(to_remove), "active-sim/%s/%d/", ent->d_name, prev_iteration);
                
                //log_info("Removing old processor directory `%s` because it is no longer needed and takes up unnecesary space.", to_remove);
                run_command("rm -rf \"%s\"", to_remove);
              }
            }
            closedir(dir); // NOTE: This is supposed to cleanup the fd as well I believe (although that was not completly clear from the documentation and if this is not the case this is a memory leak) 
            
            {
              char to_remove[4096];
              snprintf(to_remove, sizeof(to_remove), "active-sim/postProcessing/cuttingPlane/%d/", prev_iteration);
              run_command("rm -rf \"%s\"", to_remove);
            }
          }
          
          if(iter_spec.kind == ITER_SPEC_converge){
            ExtractedForces forces[MAX_CYCLIST_COUNT];
            extract_forces_for_all_cyclists(forces, "active-sim/", conf->cyclist_count);
            
            float worst_convergence = 0;
            fiz(conf->cyclist_count){
              float convergence = get_tx_force_convergence(&forces[i], 0);
              if(convergence > worst_convergence){ worst_convergence = convergence; }
            }
            
            if(worst_convergence < iter_spec.converge){
              //log_info("Got good enough convergence in stage %d, continuing to the next stage", stage);
              stage += 1;
            }else{
              log_info("We got convergence value %f which is above the required convergence of %f, continuing running...", worst_convergence, iter_spec.converge);
            }
            
            fiz(conf->cyclist_count){
              free(forces[i].sets);
            }
          }else if(iter_spec.kind == ITER_SPEC_fixed){
            stage += 1;
          }else{ assert(0); }
        }
      }
      
      if(!sim_result.success){
        log_warning("simpleFoam crashed, which indicates that the simulation diverged!");
        snprintf(result.failure_message, sizeof(result.failure_message), "simulation-simpleFoam-crashed");
      }else if(stage < stage_count){
        log_warning("Simulation did not converge within the maximum number of simpleFoam iterations we allow");
        snprintf(result.failure_message, sizeof(result.failure_message), "simulation-not-converge");
      }else{
        time_t simulation_end_time = time(NULL);
        log_info("Done running simulation in %ld seconds.", simulation_end_time - simulation_start_time);
        time_t job_end_time = time(NULL);
        
        conf->sim_runtime = (int)(simulation_end_time - simulation_start_time);
        fprintf(conf->out, "sim-runtime %d\n", conf->sim_runtime);
        fflush(conf->out);
        
        conf->total_runtime = job_end_time - job_start_time;
        fprintf(conf->out, "total-runtime %d\n", conf->total_runtime);
        fflush(conf->out);
        
        result.success = true;
      }
    }
  }
  
  return result;
}

static u8 comp_buf[4096 * 128];

static void job_step2_extract_data(JobConfiguration conf, JobStep1Result step1, char *output_dir, char *sim_dir){
  log_info("Running step 2: Data extraction (simulation = %s)", conf.name);
  
  //
  // Extract the data from the simulation into memory
  //
  
  
  ExtractedForces forces[MAX_CYCLIST_COUNT];
  extract_forces_for_all_cyclists(forces, sim_dir, conf.cyclist_count);
  
  assert(conf.cyclist_count > 0);
  //assert( == simulation_duration_in_iterations && "Intended iteration count (the directory in the simulation directory which is a base 10 number with the greatest numerical value) and actual iteration time (according to the last entry written to the forces post processing stuff) differ!");
  //int simulation_duration_in_iterations = get_latest_time_for_simulation(sim_dir); // This does work if we use the stepping rules but that is ugly
  int simulation_duration_in_iterations = forces[0].sets[forces[0].set_count - 1].time;
  //log_info("From the extracted forces we figured out that the simulation probably ran for %d iterations", simulation_duration_in_iterations);
  
  char residuals_path[4096];
  snprintf(residuals_path, sizeof(residuals_path), "%s/postProcessing/residuals/", sim_dir);
  ExtractedResiduals residuals = extract_residuals(residuals_path);
  
  float yplus_front_min = 1000000000;
  float yplus_front_max = -1000000000;
  float yplus_back_min = 1000000000;
  float yplus_back_max = -1000000000;
  float yplus_front_cyclist_avg = -INFINITY;
  float yplus_front_bike_avg = -INFINITY;
  float yplus_back_cyclist_avg = -INFINITY;
  float yplus_back_bike_avg = -INFINITY;
  { // Extract y+
    char *line = malloc(8192);
    size_t line_size = 8192;
    
    typedef struct YPlusValues YPlusValues;
    struct YPlusValues{
      char patch[256];
      int time;
      float min;
      float max;
      float average;
    };
    
    // NOTE(jkron - 2020-01-30): OpenFOAM uses a weird way to organize the postprocessing files...
    char yplus_dir_path[4096];
    snprintf(yplus_dir_path, sizeof(yplus_dir_path), "%s/postProcessing/yPlus/", sim_dir);
    char yplus_path[4096];
    snprintf(yplus_path, sizeof(yplus_path), "%s/%d/yPlus.dat", yplus_dir_path, find_greatest_number_in_directory(yplus_dir_path));
    
    //log_info("Extracting yplus (y+) values from the file `%s`...", yplus_path);
    
    FILE *f = fopen(yplus_path, "r");
    if(!f){
      panic("Unable to open yplus postprocessing file `%s`", yplus_path);
    }
    while(getline(&line, &line_size, f) != -1){
      if(line[0] != '\0' && line[strlen(line) - 1] == '\n'){ line[strlen(line) - 1] = '\0'; }
      
      if(line[0] != '#' && line[0] != '\0'){
        char *parts[256];
        int part_count = split_on_whitespace(parts, 256, line, false);
        
        if(part_count > 0){
          YPlusValues values = {0};
          values.time = atoi(parts[0]);
          copy_string(values.patch, sizeof(values.patch), parts[1]);
          values.min = atof(parts[2]);
          values.max = atof(parts[3]);
          values.average = atof(parts[4]);
          
          if(values.time == simulation_duration_in_iterations){
            if(string_equals(values.patch, "draftingPair_front_Cyklist")){ yplus_front_cyclist_avg = values.average; }
            if(string_equals(values.patch, "draftingPair_front_Cykel")){ yplus_front_bike_avg = values.average; }
            if(string_equals(values.patch, "draftingPair_back_Cyklist")){ yplus_back_cyclist_avg = values.average; }
            if(string_equals(values.patch, "draftingPair_back_Cykel")){ yplus_back_bike_avg = values.average; }
            
            if(strncmp(values.patch, "draftingPair_front_", 19) == 0 && values.min < yplus_front_min) yplus_front_min = values.min;
            if(strncmp(values.patch, "draftingPair_front_", 19) == 0 && values.max > yplus_front_max) yplus_front_max = values.max;
            if(strncmp(values.patch, "draftingPair_back_", 18) == 0 && values.min < yplus_back_min) yplus_back_min = values.min;
            if(strncmp(values.patch, "draftingPair_back_", 18) == 0 && values.max > yplus_back_max) yplus_back_max = values.max;
          }
        }
      }
    }
    fclose(f);
    free(line);
  }
  
  if(yplus_front_cyclist_avg == -INFINITY){ log_warning("WARNING: Unable to extract yplus average for the front cyclist"); }
  if(yplus_front_bike_avg == -INFINITY){ log_warning("WARNING: Unable to extract yplus average for the front bike"); }
  if(yplus_back_cyclist_avg == -INFINITY){ log_warning("WARNING: Unable to extract yplus average for the back cyclist"); }
  if(yplus_back_bike_avg == -INFINITY){ log_warning("WARNING: Unable to extract yplus average for the back bike"); }
  
  int cell_count = 0;
  { // Extract cell count
    // @HACK(jkron): The current approach is to extract the cell count from the log file from checkMesh, which seems to work (for the future: this is OpenFOAM v1906)...
    char check_mesh_log_file_path[4096];
    snprintf(check_mesh_log_file_path, sizeof(check_mesh_log_file_path), "%s/log.checkMesh", sim_dir);
    FILE *file = fopen(check_mesh_log_file_path, "r");
    if(!file){
      panic("unable to `%s` (for extracting the cell count value)", check_mesh_log_file_path);
    }
    char buf[4096];
    int len = (int)fread(buf, 1, sizeof(buf) - 1, file);
    if(len != sizeof(buf) - 1){
      log_warning("Unable to fread from log.checkMesh file oh noes probably cannot extract cell count");
    }
    buf[len] = '\0';
    fiz(len){
      // @HACK: Find substring 'nCells:', which is supposed to be followed by the number of cells.
      if(buf[i + 0] == ' ' && buf[i + 1] == 'c' && buf[i + 2] == 'e' && buf[i + 3] == 'l' && buf[i + 4] == 'l' && buf[i + 5] == 's' && buf[i + 6] == ':'){
        char *iter = buf + i + 7;
        while(*iter == ' ' || *iter == '\t') iter++;
        cell_count = atoi(iter);
        break;
      }
    }
    fclose(file);
    
    if(cell_count == 0){
      log_warning("Unable to extract cell count. Maybe something is wrong with the simulation data?");
    }
  }
  
  char in_front_of_back_cyclist_dir[4096];
  snprintf(in_front_of_back_cyclist_dir, sizeof(in_front_of_back_cyclist_dir), "%s/postProcessing/in_front_of_back_cyclist/", sim_dir);
  RectangularSample in_front_of_back_cyclist = extract_rectangular_sample(in_front_of_back_cyclist_dir, RECTANGULAR_SAMPLE_DIM_X, RECTANGULAR_SAMPLE_DIM_X);
  
  // ========================================================= //
  // Write out graphs and output data in a more compact format //
  // ========================================================= //
  
  { // Images for pressure and velocity in front of the back cyclist
    char csv_out_filename[1024];
    snprintf(csv_out_filename, sizeof(csv_out_filename), "%s/%s_in_front_of_back_cyclist_Ux.csv", output_dir, conf.name);
    
    FILE *f = fopen(csv_out_filename, "w");
    if(!f){
      panic("Unable to open the file `%s` for writing", csv_out_filename);
    }
    
    int dim_x = in_front_of_back_cyclist.width;
    int dim_y = in_front_of_back_cyclist.height;
    
    float maximum_visible_value = (conf.bike_speed + 0.5);
    
    uint8_t *pixels = calloc(dim_x * dim_y, sizeof(uint8_t));
    
    for(int y = 0; y < dim_y; ++y){
      for(int x = 0; x < dim_x; ++x){
        float Ux = in_front_of_back_cyclist.Ux[y * dim_x + x];
        
#define clamp(_minimum_, _value_, _maximum_) ((_value_) < (_minimum_) ? (_minimum_) : ( (_value_) > (_maximum_) ? (_maximum_) : (_value_) ))
        
        pixels[(dim_y - 1 - y) * dim_x + x] = (uint8_t)(pow(clamp(0, Ux, maximum_visible_value) / maximum_visible_value, 2.2) * 255.f);
        fprintf(f, "%.16f\t", Ux);
      }
      fprintf(f, "\n");
    }
    
    char image_out_filename[1024];
    snprintf(image_out_filename, sizeof(image_out_filename), "%s/%s_in_front_of_back_cyclist_Ux.png", output_dir, conf.name);
    stbi_write_png(image_out_filename, dim_x, dim_y, 1, pixels, 0);
    free(pixels);
    fclose(f);
  }
  
  { // Graph for drag and its convergence
    Composition c = {0};
    c.element_memory = comp_buf;
    c.element_capacity = sizeof(comp_buf);
    
    float x_offset = 3;
    
    float y_min = 0;
    float y_max = -10000000;
    
    float drag_convergence_min_exponent = -8;
    
    for(int i = 0, timestep = 0; i < conf.step_count; ++i){
      int stage_colours[3] = { 0xff800000, 0xff008000, 0xff000080 };
      int duration = conf.steps[i].duration;
      int stage = conf.steps[i].stage;
      push_line(&c, x_offset + timestep + 1, -10000, x_offset + timestep + 1, 10000, stage_colours[stage]);
      push_line(&c, x_offset + timestep + duration - 1, -10000, x_offset + timestep + duration - 1, 10000, stage_colours[stage]);
      
      IterSpec iter_spec = conf.iter_specs[stage];
      
      for(int i = timestep; i < timestep + duration; i += 6){
        push_line(&c,
                  i + x_offset, log(iter_spec.converge) - drag_convergence_min_exponent,
                  i + 1 + x_offset, log(iter_spec.converge) - drag_convergence_min_exponent,
                  0xff202020);
      }
      
      timestep += duration;
    }
    
    for(int i = 0; i < forces[0].set_count - 1; i += 1){
      unsigned drag_colours[2]        = { 0xffff0000, 0xff00ff00 };
      unsigned convergence_colours[2] = { 0xffaa0044, 0xff00aa44 };
      assert(conf.cyclist_count <= array_count(drag_colours));
      for(int j = 0; j < conf.cyclist_count; ++j){
        float tx = forces[j].sets[i].tx;
        float next_tx = forces[j].sets[i + 1].tx;
        
        if(i > 100){
          if(tx < y_min) y_min = tx;
          if(tx > y_max) y_max = tx;
          if(next_tx < y_min) y_min = next_tx;
          if(next_tx > y_max) y_max = next_tx;
        }
        
        push_line(&c, i + x_offset, tx, (i + 1) + x_offset, next_tx, drag_colours[j]);
        
        if(FORCE_CONVERGENCE_WINDOW_SIZE <= i){
          float convergence = log(get_tx_force_convergence(&forces[j], forces[j].set_count - i)) - drag_convergence_min_exponent;
          float next_convergence = log(get_tx_force_convergence(&forces[j], forces[j].set_count - i - 1)) - drag_convergence_min_exponent;
          
          push_line(&c, i + x_offset, convergence, (i + 1) + x_offset, next_convergence, convergence_colours[j]);
        }
      }
    }
    y_max = ceilf(y_max) + 1;
    if(y_max > 300) y_max = 300;
    if(y_min < 0) y_min = 0;
    
    for(int i = (int)floorf(y_min); i <= (int)ceilf(y_max); ++i){
      push_line(&c,
                0, i,
                (i % 10 == 0 ? 4 : 2), i,
                0xff000000);
    }
    
    PixelBuffer pb = {
      .dim_x = 512,
      .dim_y = 512,
      .stride = pb.dim_x * 4,
    };
    pb.data = calloc(pb.dim_x * pb.dim_y, 4);
    
    
    render_software(&c, &pb, (Region){ 0, y_min, x_offset + forces[0].set_count, y_max });
    
    char graph_out_filename[1024];
    snprintf(graph_out_filename, sizeof(graph_out_filename), "%s/%s_forces_graph_y_%.4f_to_%.4f.png", output_dir, conf.name, y_min, y_max);
    stbi_write_png(graph_out_filename, pb.dim_x, pb.dim_y, 4, pb.data, pb.stride);
    free(pb.data);
  }
  
  { // Graph for residuals
    Composition c = {0};
    c.element_memory = comp_buf;
    c.element_capacity = sizeof(comp_buf);
    
    float x_offset = 3;
    float y_min = 1000000;
    float y_max = -1000000;
    
    for(int i = 0; i < residuals.set_count - 1; i += 1){
      ResidualSet *set = residuals.sets + i;
      ResidualSet *next_set = residuals.sets + i + 1;
      
      // NOTE(jkron): Färger enligt viktor:
      // Tryck = Rött
      // Hastighet = Blått
      // Andra färger:
      // k = gråaktig orange
      // epsilon = gråaktig blå
      unsigned colours[4] = { 0xff0000ff, 0xffddcaae, 0xffff0000, 0xffa5b3d4 };
      ResidualField *fields[4] = { &set->U, &set->k, &set->p, &set->epsilon };
      ResidualField *next_fields[4] = { &next_set->U, &next_set->k, &next_set->p, &next_set->epsilon };
      for(int j = 0; j < array_count(fields); ++j){
        ResidualField *field = fields[j];
        ResidualField *next_field = next_fields[j];
        float value = logf(field->final[0]);
        float next_value = logf(next_field->final[0]);
        
        if(value < y_min) y_min = value;
        if(value > y_max) y_max = value;
        if(next_value < y_min) y_min = next_value;
        if(next_value > y_max) y_max = next_value;
        
        push_line(&c,
                  x_offset + i, value,
                  x_offset + (i + 1), next_value,
                  colours[j]);
      }
    }
    
    for(int i = -20; i < 20; ++i){
      push_line(&c,
                0, i,
                3, i,
                0xff000000);
    }
    
    PixelBuffer pb = {
      .dim_x = 512,
      .dim_y = 512,
      .stride = pb.dim_x * 4,
    };
    pb.data = calloc(pb.dim_x * pb.dim_y, 4);
    
    render_software(&c, &pb, (Region){ 0, y_min, x_offset + residuals.set_count, y_max });
    
    char graph_out_filename[1024];
    snprintf(graph_out_filename, sizeof(graph_out_filename), "%s/%s_residuals_graph_y_%.4f_to_%.4f.png", output_dir, conf.name, y_min, y_max);
    stbi_write_png(graph_out_filename, pb.dim_x, pb.dim_y, 4, pb.data, pb.stride);
    free(pb.data);
  }
  
  char output_csv_path[4096];
  snprintf(output_csv_path, sizeof(output_csv_path), "%s/output_v1.csv", output_dir);
  FILE *file = fopen(output_csv_path, "a");
  if(!file){
    panic("Unable to output csv file (with path `%s`)", output_csv_path);
  }
  static bool has_printed_labels = false;
  if(!has_printed_labels){
    has_printed_labels = true;
    fprintf(file, "worker\tname\tdistance\ty_offset\tbike_speed\titer_count\tfront_tx\tfront_tx_convergence\tfront_ty\tfront_tz\tback_tx\tback_tx_convergence\tback_ty\tback_tz\t1 - back_tx / front_tx\ttotal_runtime\tmesh_runtime\tsim_runtime\tcell_count\t");
    fprintf(file, "k0\tepsilon0\tRe (reynolds number)\tIt (turbulence intensity)\t");
    fprintf(file, "yplus_front_min\typlus_front_max\typlus_front_bike_avg\typlus_front_cyclist_avg\t");
    fprintf(file, "yplus_back_min\typlus_back_max\typlus_back_bike_avg\typlus_back_cyclist_avg\t");
    fprintf(file, "Ux residual\tUy residual\tUz residual\tk residual\tp residual\tepsilon residual\t");
    fprintf(file, "# entries below are from ");
    fprint_current_date(file); fprintf(file, "\n");
  }
#define TVECTOR(_f_) (_f_).sets[(_f_).set_count - 1].tx, get_tx_force_convergence(&(_f_), 0), (_f_).sets[(_f_).set_count - 1].ty, (_f_).sets[(_f_).set_count - 1].tz
#define VECTORF "%f\t%f\t%f\t%f\t"
  fprintf(file, "%s\t%s\t%f\t%f\t%f\t%d\t",
          worker_name, conf.name, conf.distance, conf.y_offset, conf.bike_speed, simulation_duration_in_iterations);
  
  fprintf(file, VECTORF, TVECTOR(forces[0]));
  if(conf.cyclist_count == 1){
    fprintf(file, "N/A\tN/A\tN/A\t");
  }else if(conf.cyclist_count == 2){
    fprintf(file, VECTORF, TVECTOR(forces[1]));
  }else{
    assert(!"no support for conf.cyclist_count other than 1 or 2");
  }
#undef TVECTOR
#undef VECTORF
  
  if(conf.cyclist_count == 2){
    fprintf(file, /*1-back_tx/front_tx*/ "%f\t", 1 - forces[1].sets[forces[1].set_count - 1].tx / forces[0].sets[forces[0].set_count - 1].tx);
  }else{
    fprintf(file, /*1-back_tx/front_tx*/ "N/A\t");
  }
  
  fprintf(file, /* runtimes */ "%d\t%d\t%d\t", conf.total_runtime, conf.mesh_runtime, conf.sim_runtime);
  fprintf(file, /* cell count */ "%d\t", cell_count);
  fprintf(file, /*k0, epsilon0, Re, It*/"%f\t%f\t%f\t%f\t", conf.k0, conf.epsilon0, conf.reynolds_number, conf.turbulence_intensity);
  fprintf(file, /* yplus front */ "%f\t%f\t%f\t%f\t", yplus_front_min, yplus_front_max, yplus_front_bike_avg, yplus_front_cyclist_avg);
  fprintf(file, /* yplus back  */ "%f\t%f\t%f\t%f\t", yplus_back_min, yplus_back_max, yplus_back_bike_avg, yplus_back_cyclist_avg);
  {
    assert(residuals.set_count > 0);
    ResidualSet *set = &residuals.sets[residuals.set_count - 1];
    
    int offsets[6] = {
      0, 1, 2,
      0,
      0,
      0,
    };
    
    ResidualField *fields[6] = {
      &set->U, &set->U, &set->U,
      &set->k,
      &set->p,
      &set->epsilon,
    };
    
    fiz(6){
      fprintf(file, /*residuals */ "%.10E\t", fields[i]->final[offsets[i]]);
    }
  }
  fprintf(file, "\n");
  fclose(file); file = NULL;
  
  { // Move all generated .vtk-files into the completed-jobs directory for further inspection.
    char job_out_dir[4096];
    snprintf(job_out_dir, sizeof(job_out_dir), "%s/%s_data/", output_dir, conf.name);
    mkdir(job_out_dir, 0777);
    
    char vtk_dir_path[4096];
    snprintf(vtk_dir_path, sizeof(vtk_dir_path), "%s/postProcessing/cuttingPlane/", sim_dir);
    
    DIR *vtk_dir = opendir(vtk_dir_path);
    if(!vtk_dir){
      panic("Could not opendir \"%s\"", vtk_dir_path);
    }
    for(struct dirent *timedir_ent; (timedir_ent = readdir(vtk_dir));){
      if(timedir_ent->d_name[0] == '.') continue;
      
      char time_dir_path[4096];
      snprintf(time_dir_path, sizeof(time_dir_path), "%s/%s/", vtk_dir_path, timedir_ent->d_name);
      
      DIR *time_dir = opendir(time_dir_path);
      if(!time_dir){
        panic("Could not opendir \"%s\"", time_dir_path);
      }
      
      for(struct dirent *ent; (ent = readdir(time_dir));){
        if(ent->d_name[0] == '.') continue;
        
        run_command("mv \"%s/%s\" \"%s/%s_%s\"",
                    time_dir_path, ent->d_name,
                    job_out_dir, timedir_ent->d_name, ent->d_name);
      }
      
      closedir(time_dir);
    }
    closedir(vtk_dir); vtk_dir = NULL;
    
    run_command("cd \"%s\" && tar --create --remove-files -z --file \"../%s_slices_data.tar.gz\" *", job_out_dir, conf.name);
    run_command("rmdir \"%s\"", job_out_dir);
  }
  
  fiz(conf.cyclist_count){
    free(forces[i].sets);
  }
  free(residuals.sets);
  free_rectangular_sample_data(in_front_of_back_cyclist);
}

static void archive_active_sim(bool failure, JobConfiguration conf){
  char *target_directory = "completed-jobs";
  if(failure){
    target_directory = "failed-jobs";
  }
  
  run_command("tar --create --file \"%s/%s.tar.gz\" --remove-files --gzip \"active-sim/\"", target_directory, conf.name);
  if(!failure){
    fprintf(conf.out, "completed-by %s ", worker_name); fprint_current_date(conf.out); fprintf(conf.out, "\n");
    fflush(conf.out);
  }
  log_info("done with job %s%s", conf.name, (failure ? " (job failed!)" : ""));
  
  // NOTE: This can fail if we extract data from a job file in completed-jobs because then the job file is already in the right place.
  run_command("mv \"%s\" \"%s/\"", conf.path, target_directory);
}

static void reextract_data(char *directory){
  DIR *dir = opendir(directory);
  if(!dir){
    panic("Unable to opendir \"%s\"", directory);
  }
  
  struct dirent *ent;
  while((ent = readdir(dir))){
    if(ent->d_name[0] == 'j' && ent->d_name[1] == 'o' && ent->d_name[2] == 'b' && ent->d_name[3] == '_'){
      int name_length = strlen(ent->d_name);
      
      if(ent->d_name[name_length - 4] == '.' && ent->d_name[name_length - 3] == 't' && ent->d_name[name_length - 2] == 'x' && ent->d_name[name_length - 1] == 't'){
        JobConfiguration conf = read_configuration(directory, ent->d_name);
        
        char archive_path[4096];
        snprintf(archive_path, sizeof(archive_path), "%s/%s.tar.gz", directory, conf.name);
        
        if(!file_exists(archive_path)){
          log_warning("Found job file `%s` without corresponding archive file. Ignoring.", conf.name);
        }else{
          char extracted_dir_path[4096];
          snprintf(extracted_dir_path, sizeof(extracted_dir_path), "%s/tmp_extracted_%s", directory, conf.name);
          
          if(run_command("mkdir \"%s\" && cd \"%s\" && tar --extract --file \"../%s\" --gzip", extracted_dir_path, extracted_dir_path, archive_path) != 0){
            panic("Unable to extract tar archive! Exiting!!!");
          }
          
          char sim_dir[4096];
          snprintf(sim_dir, sizeof(sim_dir), "%s/active-sim/", extracted_dir_path);
          char out_dir[4096];
          snprintf(out_dir, sizeof(out_dir), "%s/out/", directory);
          mkdir(out_dir, 0777);
          JobStep1Result step1 = {0};
          job_step2_extract_data(conf, step1, out_dir, sim_dir);
          
          run_command("rm -r \"%s\"", extracted_dir_path);
        }
        
        free_job_conf(&conf);
      }
    }else{
      log_warning("File `%s` not recognized as a job file. Ignoring.", ent->d_name);
    }
  }
  
  closedir(dir);
}

static void handle_ctrl_c(int s){
  ctrl_c_detected = true;
}

/* returns true if all jobs in the pending-jobs/ directory were processed. Otherwise false is returned. */
static bool do_all_pending_jobs(int *job_index, int num_jobs_to_run, time_t no_new_jobs_after_time){
  bool result = false;
  
  char pending_jobs_path[4096]; snprintf(pending_jobs_path, sizeof(pending_jobs_path), "%s/pending-jobs/", auto_directory_path);
  DIR *jobs_dir = opendir(pending_jobs_path);
  if(!jobs_dir){ panic("Unable to open the `%s` directory!!!!!!!! SOMETHING IS VERY WRONG! EXITING!", pending_jobs_path); }
  for(; jobs_dir; *job_index += 1){
    {
      if(*job_index >= num_jobs_to_run){
        log_warning("Hit upper limit on the number of jobs to run, exiting.");
        break;
      }
      time_t timestamp = time(NULL);
      if(timestamp > no_new_jobs_after_time){
        log_warning("Hit the time were we are not supposed to start any new jobs.");
        break;
      }
    }
    
    struct dirent *ent = readdir(jobs_dir);
    while(ent){
      int name_len = strlen(ent->d_name);
      if(ent->d_name[0] == 'j' && ent->d_name[1] == 'o' && ent->d_name[2] == 'b' && ent->d_name[name_len - 4] == '.' && ent->d_name[name_len - 3] == 't' && ent->d_name[name_len - 2] == 'x' && ent->d_name[name_len - 1] == 't'){
        break;
      }else if(!(ent->d_name[0] == '.' && (ent->d_name[1] == 0 || (ent->d_name[1] == '.' && ent->d_name[2] == 0)))){
        log_warning("Found weird file `%s` in the `%s` directory that does not seem like it is a job (it is not on the form 'job<whatever>.txt)", ent->d_name, pending_jobs_path);
      }
      ent = readdir(jobs_dir);
    }
    if(ent){
      {
        struct statvfs buf;
        int status = statvfs(".", &buf);
        if(status == -1){
          log_warning("statvfs failed: cannot get the amount of free disk space!!!");
        }else{
          u64 free = (u64)buf.f_bsize * (u64)buf.f_bfree;
          
          //log_info("... statvfs reports that there is %.2fGib of free space", free / (1024.0 * 1024.0 * 1024.0));
          
          u64 simulation_requirement = (u64)1024 * 1024 * 8192;
          if(free < simulation_requirement){
            panic("We require that the amount of free disk space exceeds %.2fGib, but there is only %.2fGib of free space available on the file system!", simulation_requirement / (1024.0 * 1024.0 * 1024.0), free / (1024.0 * 1024.0 * 1024.0));
          }
        }
      }
      
      JobConfiguration conf = read_configuration(pending_jobs_path, ent->d_name);
      log_info("Selected job %s in directory %s", conf.name, pending_jobs_path);
      write_work_shift_entry("begin_job", conf.name);
      
      bool failure = false;
      fprintf(conf.out, "taken-by %s ", worker_name); fprint_current_date(conf.out); fprintf(conf.out, "\n");
      fflush(conf.out);
      
      JobStep1Result step1 = job_step1_prepare_and_run_simulation(&conf);
      if(!step1.success){
        failure = true;
        fprintf(conf.out, "failure %s %s ", step1.failure_message, worker_name); fprint_current_date(conf.out); fprintf(conf.out, "\n");
        fflush(conf.out);
      }else{
        job_step2_extract_data(conf, step1, "completed-jobs/", "active-sim/");
      }
      
      archive_active_sim(failure, conf);
      
      write_work_shift_entry("end_job", conf.name);
      free_job_conf(&conf);
    }else{
      result = true;
      log_info("Done processing all jobs in the `%s` directory.", pending_jobs_path);
      closedir(jobs_dir); jobs_dir = NULL;
    }
  }
  return result;
}


static void print_usage(char **argv){
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage 1: %s COMPUTER-NAME OPTIONS...\n", argv[0]);
  fprintf(stderr, "  COMPUTER-NAME - The name used to identify this computer (e.g. 'tybalt', 'bidoof', 'joldlaptop').\n");
  
  fprintf(stderr, "   --num-jobs N, State the maximum number of jobs to run (defaults to running all jobs)\n");
  fprintf(stderr, "   --no-new-sims-after HH:MM, Only start new simulations before hitting this clock time.\n");
  fprintf(stderr, "\n");
  
  fprintf(stderr, "Usage 2: %s COMPUTER-NAME --finalize-active-sim\n", argv[0]);
  fprintf(stderr, "   --finalize-active-sim DO_ARCHIVE JOB_DIRECTORY JOB_FILENAME, Extract data from and archive the active-sim/ directory and exit directly afterwards in addition to taking care of the job file. The 'active-sim/' simulation is assumed have been run successfully. This mode of operation is intended for finalizing simulations that have manually been fixed in some way and is not part of the intended workflow. If the DO_ARCHIVE variable is equal to \"true\" then the simulation will be archived and if it is equal to \"false\" will not be archived.\n");
  
  fprintf(stderr, "\n");
  fprintf(stderr, "Usage 3: %s COMPUTER-NAME --reextract-data DIRECTORY\n", argv[0]);
  fprintf(stderr, "   --reextract-data DIRECTORY, Extracts data from all simulation archives in a directory (based on the presence of job files).\n");
  fprintf(stderr, "\n");
}

int main(int argc, char **argv){
  { // NOTE(jkron - 2020-01-16): Handle CTRL-C properly.
    struct sigaction action;
    action.sa_handler = handle_ctrl_c;
    sigemptyset(&action.sa_mask);
    action.sa_flags = 0;
    sigaction(SIGINT, &action, NULL);
  }
  
  { // NOTE(jkron - 2020-01-16): Enable core dumps (based on answers on https://stackoverflow.com/questions/17965/how-to-generate-a-core-dump-in-linux-on-a-segmentation-fault ). See man CORE(5) for more info.
    struct rlimit limit = {0};
    limit.rlim_cur = 1024 * 1024 * 1024; // 1Gib maximum core dump file size
    limit.rlim_max = RLIM_INFINITY;
    
    setrlimit(RLIMIT_CORE, &limit);
  }
  
  {
    time_t timestamp = time(NULL);
    struct tm *time = localtime(&timestamp);
    
    char log_path[4096];
    mkdir("logs/", 0777);
    snprintf(log_path, sizeof(log_path), "logs/run-jobs_%04d-%02d-%02d_%02d:%02d:%02d.log", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday, time->tm_hour, time->tm_min, time->tm_sec);
    
    _log.file = fopen(log_path, "w");
    if(!_log.file){
      panic("Unable to create the log file `%s`", log_path);
    }
  }
  
  {
    auto_directory_path = get_current_dir_name();
    if(!auto_directory_path){
      panic("get_current_dir_name() - failed!");
    }
  }
  
  int num_jobs_to_run = INT32_MAX;
  time_t no_new_sims_after_time = INT64_MAX;
  bool finalize_active_sim = false;
  bool finalize_active_sim_do_archive = false;
  char *finalize_active_sim_job_directory = NULL;
  char *finalize_active_sim_job_filename = NULL;
  if(argc > 1){
    worker_name = argv[1];
    char **arg = argv + 2;
    if(string_equals(arg[0], "--finalize-active-sim")){
      finalize_active_sim = true;
      finalize_active_sim_do_archive = parse_bool_string(arg[1]);
      finalize_active_sim_job_directory = arg[2];
      finalize_active_sim_job_filename = arg[3];
      if(!arg[1] || !arg[2] || !arg[3]){
        print_usage(argv);
        shutdown(1);
      }
    }else if(string_equals(arg[0], "--reextract-data")){
      char *directory = arg[1];
      if(!directory){
        print_usage(argv);
        shutdown(1);
      }
      
      reextract_data(directory);
      shutdown(0);
    }else while(arg < argv + argc){
      if(string_equals(arg[0], "--num-jobs")){
        num_jobs_to_run = atoi(arg[1]);
        arg += 2;
      }else if(string_equals(arg[0], "--no-new-sims-after")){
        char *date_string = arg[1];
        
        time_t timestamp = time(NULL);
        struct tm *time = localtime(&timestamp);
        
        time_t current_seconds_into_day = time->tm_hour * 3600 + time->tm_min * 60 + time->tm_sec;
        
        int hour;
        int minute;
        sscanf(date_string, "%02d:%02d", &hour, &minute);
        
        assert(0 <= hour && hour < 24);
        assert(0 <= minute && minute < 60);
        
        time_t seconds_into_day = hour * 3600 + minute * 60;
        
        if(seconds_into_day > current_seconds_into_day){
          no_new_sims_after_time = timestamp - current_seconds_into_day + seconds_into_day;
        }else{
          no_new_sims_after_time = timestamp - current_seconds_into_day + seconds_into_day + 24 * 3600;
        }
        log_warning("NOTE: The current unix time is %ld and we will not start any new simulations after %ld i.e. we will only start simulations for the next %ld seconds", timestamp, no_new_sims_after_time, no_new_sims_after_time - timestamp);
        
        arg += 2;
      }else if(string_equals(arg[0], "-h") && string_equals(arg[0], "--help")){
        print_usage(argv);
        shutdown(0);
      }else{
        print_usage(argv);
        shutdown(1);
      }
    }
  }else{
    print_usage(argv);
    shutdown(1);
  }
  
  { // NOTE: Small environment sanity check
    DIR *dir = opendir("pending-jobs/");
    if(!dir){
      panic("The pending-jobs/ folder doesn't exist!!!! (maybe you tried to run this program with the wrong working directory?)");
    }
    closedir(dir);
    dir = opendir("failed-jobs/");
    if(!dir){
      panic("The failed-jobs/ folder doesn't exist! (But pending-jobs/ does exist, so you are probably in the correct working directory. It will probably suffice to simply create the failed-jobs/ directory to resolve this problem.)");
    }
    closedir(dir);
    dir = opendir("completed-jobs/");
    if(!dir){
      panic("The completed-jobs/ folder doesn't exist! (But pending-jobs/ and failed-jobs/ does exist, so you are probably in the correct working directory. It will probably suffice to simply create the completed-jobs/ directory to resolve this problem.)");
    }
    closedir(dir);
  }
  
  if(finalize_active_sim){
    if(!file_exists("active-sim/")){
      panic("Running in --finalize-active-sim mode but there is no `active-sim/` directory present!");
    }
    
    log_info("Performing test data extraction from `active-sim/`");
    JobConfiguration conf = read_configuration(finalize_active_sim_job_directory, finalize_active_sim_job_filename);
    JobStep1Result step1 = {0};
    job_step2_extract_data(conf, step1, "completed-jobs/", "active-sim/");
    if(finalize_active_sim_do_archive){
      archive_active_sim(/*failure=*/false, conf);
    }
    log_info("Done! (active-sim/ should have been finalized and archived at this point.)");
    free_job_conf(&conf);
  }else{
    //
    // NOTE: The normal mode of operation for the program.
    //
    if(file_exists("active-sim/")){
      panic("There is already an `active-sim/` directory present. Do something about it!\n");
    }
    
    {
      char *work_shift_path = "work_shifts.csv";
      bool created_work_shift_file = !file_exists(work_shift_path);
      work_shift_csv = fopen(work_shift_path, "a");
      if(!work_shift_csv){
        panic("Unable to open the file `%s` for writing", work_shift_path);
      }
      if(created_work_shift_file){
        fprintf(work_shift_csv, "Date\tUnix timestamp\tWorker name\tOperation\tOperation data\n"); fflush(work_shift_csv);
      }
    }
    write_work_shift_entry("start", "");
    
    log_info("Now doing all jobs already in the pending-jobs/ directory...");
    int job_index = 0;
    bool do_more = do_all_pending_jobs(&job_index, num_jobs_to_run, no_new_sims_after_time);
    
    if(do_more){
      log_info("Now processing the batches...");
      while(true){
        bool did_something = false;
        
        DIR *batches_dir = opendir("batches/");
        if(!batches_dir){
          panic("Could not open the batches/ directory");
        }
        for(struct dirent *batch_dirent; (batch_dirent = readdir(batches_dir)) && do_more;){
          while(batch_dirent && batch_dirent->d_name[0] == '.') batch_dirent = readdir(batches_dir);
          
          if(batch_dirent && !string_equals(batch_dirent->d_name, "README.txt")){
            did_something = true;
            char batch_path[1024];
            snprintf(batch_path, sizeof(batch_path), "%s/batches/%s", auto_directory_path, batch_dirent->d_name);
            log_info("Extracting files from batch file `%s`", batch_path);
            
            if(run_command("cd pending-jobs/ && tar --extract --gzip --file %s", batch_path) == 0){
              if(run_command("mv \"%s\" processed_batches/", batch_path) != 0){
                panic("Unable to move the batch file we just extracted out of the way. We exited now in order to make sure that those jobs will not be run over and over and over. This should not happen.");
              }
              do_more = do_all_pending_jobs(&job_index, num_jobs_to_run, no_new_sims_after_time);
            }else{
              log_warning("Unable to extract jobs from batch file `%s`", batch_path);
            }
          }
        }
        closedir(batches_dir);
        
        if(!did_something){
          break;
        }
      }
      log_info("... done processing all files in the batches/ directory.");
    }
    
    write_work_shift_entry("stop", "");
  }
  
  return 0;
}
