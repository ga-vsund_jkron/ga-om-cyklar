/*
daemonize.c - A small program that runs other programs as daemons

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>

static void my_close(int *fd){
    close(*fd);
    *fd = -1;
}

#define FILE_MODE (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)

#define close Use my_close instead!

// if output_file is NULL then use output_fd instead
static int daemonize(char **argv, char *output_file, char *pid_file){
    int pid_fd = -1;
    if(pid_file != NULL){
        pid_fd = open(pid_file, O_CREAT | O_EXCL | O_WRONLY, FILE_MODE);
        if(pid_fd == -1){
            int errno_value = (errno);
            if(errno_value == EEXIST){
                fprintf(stderr, "FATAL ERROR: Pid file %s already exists. This could mean another instance of this software is already running. Delete the pid file to resolve this problem.\n", pid_file);
            }else{
                fprintf(stderr, "FATAL ERROR: Could not open pid file %s: %s\n", pid_file, strerror(errno_value));
            }
            exit(1);
        }
    }

    int error = 0;

    pid_t fork_pid = fork();
    if(fork_pid == -1){
        fprintf(stderr, "FATAL ERROR: Couldn't fork\n");
        error = 1;
    }else if(fork_pid == 0){ // Child
        if(setsid() < 0){ // I don't know what this does but daemons are suppossed to call this function for some reason.
            fprintf(stderr, "FATAL ERROR: setsid() failed!\n");
            error = 1;
        }else{
            fork_pid = fork(); // I have no idea why you are suppossed to fork twice but you are supposed to do that in daemons...
            if(fork_pid == -1){
                fprintf(stderr, "FATAL ERROR: Couldn't fork a second time\n");
                error = 1;
            }else if(fork_pid == 0){
                int output_fd = -1;
                if(output_file){
                    output_fd = open(output_file, O_CREAT | O_APPEND | O_WRONLY, FILE_MODE);
                    if(output_fd == -1){
                        fprintf(stderr, "FATAL ERROR: Unable to open file %s: %s\n", output_file, strerror(errno));
                        error = 1;
                    }
                }

                char *input_file = "/dev/null";
                int input_fd = open(input_file, O_RDONLY, FILE_MODE);
                if(input_fd == -1){
                    fprintf(stderr, "FATAL ERROR: Unable to open file %s: %s\n", input_file, strerror(errno));
                    error = 1;
                }

                if(!error){
                    // Replace standard out and standard error
                    int result0 = dup2(input_fd, STDIN_FILENO);
                    int result1 = dup2(output_fd, STDOUT_FILENO);
                    int result2 = dup2(output_fd, STDERR_FILENO);
                    if(result0 == -1 || result1 == -1 || result2 == -1){
                        fprintf(stderr, "FATAL ERROR: Cannot redirect stdout, stdin and stderr to a file!\n");
                        error = 1;
                    }else{
                        my_close(&output_fd);
                        my_close(&input_fd);

                        fprintf(stderr, "---------------------------\n");
                        fprintf(stderr, "- DAEMONIZE: NEW INSTANCE -\n");
                        fprintf(stderr, "---------------------------\n");

                        fork_pid = fork(); // We need to fork one more time because we need one proccess that cleans up after the daemonizee
                        if(fork_pid == -1){
                            fprintf(stderr, "FATAL ERROR: Couldn't fork a third time\n");
                            error = 1;
                        }else if(fork_pid == 0){ // Child
                            if(pid_file != NULL){ // Write pid to file
                                pid_t this_pid = getpid();
                                char pid_buf[256];
                                int pid_buf_len = snprintf(pid_buf, sizeof(pid_buf), "%ld", (long)this_pid);
                                if(pid_buf_len == -1 || pid_buf_len >= sizeof(pid_buf)){
                                    fprintf(stderr, "FATAL ERROR: nsprintf failed for some weird reason!\n");
                                    error = 1;
                                    my_close(&pid_fd);
                                }else{
                                    ssize_t write_pid_result = write(pid_fd, pid_buf, pid_buf_len);
                                    if(write_pid_result == -1){
                                        fprintf(stderr, "FATAL ERROR: Could not write pid to pid file\n");
                                        error = 1;
                                    }else{//success
                                        my_close(&pid_fd);
                                    }
                                }
                            }

                            if(!error){
                                fprintf(stderr, "Child: Calling execv\n");
                                execv(argv[0], argv);
                                int errno_value = (errno);

                                fprintf(stderr, "Child: FATAL ERROR: execve failed! errno = %s\n", strerror(errno_value));
                                error = 1;
                            }
                        }
                        else{ // Parent
                            pid_t child_pid = wait(NULL);
                            if(child_pid == -1){
                                int errno_value = (errno);
                                fprintf(stderr, "Call to wait() failed. errno = %s\n", strerror(errno_value));
                            }else{
                                // Success!
                            }
                        }
                    }
                }
            }else{ // Parent
                if(pid_fd != -1){ my_close(&pid_fd); }
            }
        }
    }else{ // Parent: exit so child gets inherited by init
        if(pid_fd != -1){ my_close(&pid_fd); }
    }
    if(pid_fd != -1){
        my_close(&pid_fd);
        if(unlink(pid_file) == -1){
            int errno_value = (errno);
            fprintf(stderr, "Cannot remove pid file %s. errno=%s\n", pid_file, strerror(errno_value));
        }
    }
    return(error);
}

int main(int argc, char **argv){
    int i;
    char **child_argv = NULL;

    char *output_file = "/dev/null";
    char *pid_file = NULL;

    int help = 0;

    for(i = 1; i < argc; ++i){
        if(argv[i][0] == '-'){
            if(argv[i][1] == 'h'){
                help = 1;
                goto usage;
            }else if(argv[i][1] == 'p'){
                if(i + 1 < argc){
                    i += 1;
                    pid_file = argv[i];
                }else{
                    goto usage;
                }
            }else if(argv[i][1] == 'o'){
                if(i + 1 < argc){
                    i += 1;
                    output_file = argv[i];
                }else{
                    goto usage;
                }
            }else{
                goto usage;
            }
        }else{
            child_argv = argv + i;
            break;
        }
    }
    if(!child_argv){
        usage:
        fprintf(stderr, "Usage: %s [-p pidfile] [-o logfile] proccess [arguments]\n", argv[0]);
        exit(help == 1 ? 0 : 1);
    }

    return daemonize(child_argv, output_file, pid_file);
}

