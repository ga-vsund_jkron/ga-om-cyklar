#!/bin/bash
cd ${0%/*} || exit 1                        # Run from this directory

if [[ ! -f $1 ]] ; then
  echo "Usage: $0 <COMPRESSED SIMULATION ARCHIVE FILE>"
  echo "the provided file (if any) does not exist!!!!"
  exit 1
fi

archive=`realpath $1`

#tmp=`mktemp --directory`
tmp="investigation"
if [[ -d $tmp ]] ; then
  echo "[investigate.sh] Directory $tmp is non-empty, not doing anything!"
  exit 0
fi
mkdir -p $tmp
cd $tmp
echo "[investigate.sh] Doing stuff in $tmp"

tar -xzf "$archive"
if [[ ! $? -eq 0 ]]; then
  echo "Fatal error: tar failed!"
  exit 1
fi

cd active-sim/
echo "[investigate.sh] Preparing for viewing with paraview..."
docker start of_v1906
if [[ ! $? -eq 0 ]]; then
  echo "[investigate.sh] Fatal error: Unable to start the docker container we want :("
  exit 1
fi

if [[ ! -f ./prepare_for_paraview.sh ]] ; then
  echo "[investigate.sh] WARNING: (remove this when no longer doin' old sims) prepare_for_paraview.sh script missing, getting it frrom the simulation template"
  cp ../../sim-template-k-epsilon/prepare_for_paraview.sh .
fi

docker exec -t -w "`pwd`/" of_v1906 /bin/bash -rcfile /opt/OpenFOAM/setImage_v1906.sh -c ./prepare_for_paraview.sh
if [[ ! $? -eq 0 ]]; then
  echo "[investigate.sh] Fatal error: It looks like we failed running prepare_for_paraview.sh inside the OpenFOAM docker container :(. Exiting now!"
  exit 1
fi
sync

echo "[investigate.sh] Here is the log file for reconstructParMesh:"
cat log.reconstructParMesh
echo "[investigate.sh] Here is the log file for reconstructPar:"
cat log.reconstructPar

echo "[investigate.sh] Now running paraview!"
touch etwas.foam
paraview etwas.foam

