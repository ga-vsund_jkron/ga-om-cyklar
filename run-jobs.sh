#!/bin/bash

gcc run-jobs.c -lm -o run-jobs-executable -g
if [[ ! $? -eq 0 ]] ; then
  echo "run-jobs.c failed to compile :-("
  exit 1
fi
./run-jobs-executable

