/*
graphite.c - Code for rendering simple things (for scientific computing related stuff?)
Created: 2019-12-21
Author: jkron

*/

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int64_t i64;
typedef int32_t i32;
typedef float f32;

typedef struct Element Element;
struct Element{
#define ELEMENT_line 1
#define ELEMENT_text 2
  int kind;
};

typedef struct Element_Line Element_Line;
struct Element_Line{
  f32 x0, y0;
  f32 x1, y1;
  u32 color;
};

typedef struct Element_Text Element_Text;
struct Element_Text{
  f32 x0, y0;
  u32 length;
  char string[0];
};

typedef struct Composition Composition;
struct Composition{
  u8 *element_memory;
  u32 element_cursor;
  u32 element_capacity;
};

static void* _push_element(Composition *c, int kind, size_t size){
  void *result = 0;
  if(c->element_cursor + sizeof(Element) + size <= c->element_capacity){
    Element *element = (Element*)(c->element_memory + c->element_cursor);
    c->element_cursor += sizeof(Element);
    element->kind = kind;
    result = c->element_memory + c->element_cursor;
    c->element_cursor += size;
    assert(c->element_cursor <= c->element_cursor);
  }
  return result;
}

static void push_line(Composition *c, f32 x0, f32 y0, f32 x1, f32 y1, u32 color){
  Element_Line *line = _push_element(c, ELEMENT_line, sizeof(Element_Line));
  if(line){
    line->x0 = x0;
    line->y0 = y0;
    line->x1 = x1;
    line->y1 = y1;
    line->color = color;
  }
}
static void push_text(Composition *c, f32 x0, f32 y0, char *string, size_t length){
  Element_Text *text = _push_element(c, ELEMENT_text, sizeof(Element_Text) + length);
  if(text){
    text->x0 = x0;
    text->y0 = y0;
    assert(length < UINT32_MAX);
    text->length = length;
    memcpy(text->string, string, length);
  }
}

typedef struct PixelBuffer PixelBuffer;
struct PixelBuffer{
  u8 *data;
  u32 dim_x;
  u32 dim_y;
  u32 stride; // NOTE: distance between the first element of two consecutive rows, in bytes
};

typedef struct Region Region;
struct Region{ f32 x0, y0, x1, y1; };
typedef struct Vec Vec;
struct Vec{ f32 x, y; };

static void map_into_pb(PixelBuffer *pb, Region region, f32 *x, f32 *y){
  *x = (*x - region.x0) * (f32)pb->dim_x / (region.x1 - region.x0);
  *y = (*y - region.y1) * (f32)pb->dim_y / (region.y0 - region.y1);
}
static void normalize(f32 *x, f32 *y){
  f32 l = *x * *x + *y * *y;
  l = sqrtf(l);
  if(l > 0.0001){
    l = 1 / l;
    *x *= l;
    *y *= l;
  }
}
static void swap(f32 *a, f32 *b){
  f32 t = *a;
  *a = *b;
  *b = t;
}

static i64 f32_to_i64(f32 x){
  i64 result = 0;
  if(x > 0){
    result = (i64)(x + 0.5f);
  }else{
    result = (i64)(x - 0.5f);
  }
  return result;
}

static void write_pixel(PixelBuffer *pb, i64 x, i64 y, u32 value){
  if(0 <= x && x < pb->dim_x && 0 <= y && y < pb->dim_y){
    *(u32*)(pb->data + x * 4 + y * pb->stride) = value;
  }
}

static int sign_of_f32(f32 x){
  int result = 0;
  if(x < 0){
    result = -1;
  }else if(x > 0){
    result = 1;
  }
  return result;
}

static void render_software(Composition *c, PixelBuffer *pb, Region region){
  for(size_t cursor = 0; cursor < c->element_cursor;){
    Element *element = (Element*)(c->element_memory + cursor);
    cursor += sizeof(*element);
    switch(element->kind){
      case ELEMENT_line:{
        Element_Line *line = (Element_Line*)(c->element_memory + cursor);
        cursor += sizeof(*line);
        
        f32 x0 = line->x0;
        f32 y0 = line->y0;
        map_into_pb(pb, region, &x0, &y0);
        f32 x1 = line->x1;
        f32 y1 = line->y1;
        map_into_pb(pb, region, &x1, &y1);
        
#if 0
        
        // TODO(jkron - 2020-01-13): Work in progress!
        
        f32 cur_x = x0;
        f32 cur_y = y0;
        
        f32 next_x = clamp(floorf(x0), x1, ceilf(x0));
        if(next_x == x0){ next_x = x0 + sign_of_f32(x1 - x0); }
        f32 next_y = clamp(floorf(y0), y1, ceilf(y0));
        if(next_y == y0){ next_y = y0 + sign_of_f32(y1 - y0); }
        
        if((next_x - x0) * (y1 - y0) > (next_y - y0) * (x1 - x0)){
          
        }
        
#else // Naive pixelated version
        if(x1 < x0){
          swap(&x0, &x1);
          swap(&y0, &y1);
        }
        
        f32 dx = x1 - x0;
        f32 dy = y1 - y0;
        normalize(&dx, &dy);
        
        if(dx > 0.001){
          f32 x = x0;
          f32 y = y0;
          for(; x < x1; x += dx, y += dy){
            write_pixel(pb, f32_to_i64(x), f32_to_i64(y), line->color);
          }
        }else{
          if(dy < 0){
            swap(&x0, &x1);
            swap(&y0, &y1);
            dy = -dy;
            dx = -dx;
          }
          
          // HACK(jkron - 2020-02-23): If too big numbers are feed into this algorithm,
          // the numbers dx or dy will be outside of the precision of the numbers and
          // consequently this loop may run forever. To quickly fix this problem I
          // restricted the maximum amount of iterations in the loop instead of addressing
          // the problem more directly by looking at the floating point numbers.
          int counter = 0;
          f32 x = x0;
          f32 y = y0;
          for(; y < y1 && counter < 10000; x += dx, y += dy, counter += 1){
            write_pixel(pb, f32_to_i64(x), f32_to_i64(y), line->color);
          }
        }
#endif
      }break;
      
      case ELEMENT_text:{
        Element_Text *text = (Element_Text*)(c->element_memory + cursor);
        cursor += sizeof(*text) + text->length;
        fprintf(stderr, "@TODO: Render text!\n");
      }break;
      
      default:{
        assert(!"Unknown element type!");
      }break;
    }
  }
}

#if 0
//
// NOTE: Demo program
//

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include <stdlib.h>
#define __USE_POSIX199309
#include <time.h>

static uint64_t get_time_us(){
  uint64_t result = 0;
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  result = (uint64_t)ts.tv_sec * 1000000 + (uint64_t)ts.tv_nsec / 1000;
  return result;
}

static f32 f(f32 t){
  return t * t * 0.05f;
}

int main(int argc, char **argv){
  
  static u8 comp_buf[4096 * 32];
  
  Composition c = {
    .element_memory = comp_buf,
    .element_capacity = sizeof(comp_buf),
  };
  
#if 0
  push_line(&c, 1, 1, 10, 1, 0xffcccccc);
  push_line(&c, 10, 1, 10, 10, 0xffcccccc);
  push_line(&c, 10, 10, 1, 10, 0xffcccccc);
  push_line(&c, 1, 10, 1, 1, 0xffcccccc);
#endif
  for(int i = 0; i < 11; i += 1){
    push_line(&c, i, f(i), i + 1, f(i + 1), 0xff00ff00);
  }
  
  PixelBuffer pb = {
    .dim_x = 128,
    .dim_y = 128,
    .stride = pb.dim_x * 4,
  };
  pb.data = calloc(pb.dim_x * pb.dim_y, sizeof(u32));
  
  u64 render_start = get_time_us();
  render_software(&c, &pb, (Region){ 0, 0, 11, 11 });
  u64 render_end = get_time_us();
  fprintf(stderr, "Rendered in %.3fms\n", (render_end - render_start) / 1000.0);
  
  stbi_write_png("out.graphite.png", pb.dim_x, pb.dim_y, 4, pb.data, pb.stride);
  
  return 0;
}
#endif
