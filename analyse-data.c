/*
analyse-data.c - Analyse the output from run-jobs.c
Created: 2020-02-17
Author: jkron

*/

static void panic(char *format, ...);

#include "utility.c"

static void panic(char *format, ...){
  va_list list;
  
  fprintf(stderr, "\n");
  fprintf(stderr, "\n");
  fprintf(stderr, "\n");
  fprintf(stderr, ANSI_COLOR_RED);
  fprintf(stderr, "[analyse-data.c ");
  fprint_current_date(stderr);
  fprintf(stderr, " FATAL ERROR] ");
  fprintf(stderr, ANSI_COLOR_RESET);
  va_start(list, format);
  vfprintf(stderr, format, list);
  va_end(list);
  fprintf(stderr, "\n");
  fflush(stderr);
  
  __asm__("int3");
  
  exit(1);
}

#define D_INV_GLATTNINGSFAKTOR 0.2
#define D_LOG_GLATTNINGSFAKTOR 0.5

typedef struct Coeffs Coeffs;
struct Coeffs{
#define N 8
  float C[N];
};
#define FCOEFFS(_coeffs_) (_coeffs_).C[0], (_coeffs_).C[1], (_coeffs_).C[2], (_coeffs_).C[3], (_coeffs_).C[4], (_coeffs_).C[5], (_coeffs_).C[6], (_coeffs_).C[7]

typedef struct Sample Sample;
struct Sample{
  // Independent variables:
  double log_d;
  double _d; // Original value of d
  double s;
  //double U; // AKA v_cykel
  
  // Dependent variables:
  //double drag_back;
  double gain;
};

typedef struct Samples Samples;
struct Samples{
  Sample *values;
  int count;
};


#if 0
static double f(Coeffs coeffs, Sample sample){
  double result = (coeffs.C[0] + coeffs.C[1] * sample.log_d) * exp(coeffs.C[3] * square(sample.s - coeffs.C[2]));
  return result;
}

static Coeffs calculate_gradient(Coeffs coeffs, Samples samples){
#if 0
  NOTE OUTDATED!!!!!!;
  f(A, B, C, D) = (A + B log(d)) e^(D (s - C)^2);
  NOTE OUTDATED!!!!!!;
  Grad((A + B log(d)) e^(D (s - C)^2));
  NOTE OUTDATED!!!!!!;
  d/dA((A + B log(d)) e^(D (s - C)^2)) =                e^(D (s - C)^2);
  d/dB((A + B log(d)) e^(D (s - C)^2)) = log(d)         e^(D (s - C)^2);
  d/dC((A + B log(d)) e^(D (s - C)^2)) = -2 D (s - C)   e^(D (s - C)^2) (A + B log(d));
  d/dD((A + B log(d)) e^(D (s - C)^2)) =      (s - C)^2 e^(D (s - C)^2) (A + B log(d));
  NOTE OUTDATED!!!!!!;
  Kvadratiska felet över alla samples blir:
  K(A, B, C, D) = SUMMERA ÖVER ALLA SAMPLES: 2(f(A, B, C, D) - F_D) Grad(f(A, B, C, D));
#endif
  Coeffs result = {0};
  
  fiz(samples.count){
    Sample sample = samples.values[i];
    
    double first_two_coeffs_part = (coeffs.C[0] + coeffs.C[1] * sample.log_d);
    double exponential_part = exp(coeffs.C[3] * square(sample.s - coeffs.C[2]));
    double Q = 2 * (f(coeffs, sample) - sample.gain) * exponential_part;
    
    result.C[0] += Q;
    result.C[1] += Q * sample.log_d;
    result.C[2] += Q * -2 * coeffs.C[3] * (sample.s - coeffs.C[2]) * first_two_coeffs_part;
    result.C[3] += Q * square(sample.s - coeffs.C[2]) * first_two_coeffs_part;
  }
  
  return result;
}
#else

static double f(Coeffs coeffs, Sample sample){
  double result = (coeffs.C[0] + coeffs.C[1] * sample.log_d) * exp((coeffs.C[3] + sample._d * coeffs.C[4] + 1/(coeffs.C[6] + sample._d) * coeffs.C[5]) * square(sample.s - coeffs.C[2]));
  return result;
}

static Coeffs calculate_gradient(Coeffs coeffs, Samples samples){
  Coeffs result = {0};
  
  fiz(samples.count){
    Sample sample = samples.values[i];
    
    double first_two_coeffs_part = (coeffs.C[0] + coeffs.C[1] * sample.log_d);
    double exponential_part = exp((coeffs.C[3] + sample._d * coeffs.C[4] + coeffs.C[5] * 1/(coeffs.C[6] + sample._d)) * square(sample.s - coeffs.C[2]));
    double Q = 2 * (f(coeffs, sample) - sample.gain) * exponential_part;
    
    result.C[0] += Q;
    result.C[1] += Q * sample.log_d;
    result.C[2] += Q * -2 * (coeffs.C[3] + coeffs.C[4] * sample._d + coeffs.C[5] * 1/(coeffs.C[6] + sample._d)) * (sample.s - coeffs.C[2]) * first_two_coeffs_part;
    result.C[3] += Q * square(sample.s - coeffs.C[2]) * first_two_coeffs_part;
    result.C[4] += sample._d * Q * square(sample.s - coeffs.C[2]) * first_two_coeffs_part;
    result.C[5] += (1 / (sample._d + 0.1)) * Q * square(sample.s - coeffs.C[2]) * first_two_coeffs_part;
    result.C[6] += 0; // This one is fixed at its initial value.
    result.C[7] += 0; // This one is fixed at its initial value.
  }
  
  return result;
}
#endif

static double calculate_cost(Coeffs coeffs, Samples samples){
  double result = 0;
  fiz(samples.count){
    Sample sample = samples.values[i];
    double cost = sample.gain - f(coeffs, sample);
    result += square(cost) / (double)samples.count;
  }
  return result;
}

typedef struct GradientDescentResult GradientDescentResult;
struct GradientDescentResult{
  Coeffs optimal_coeffs;
  double optimal_cost;
  int step_count;
};

static bool is_valid_f64(double value){
  bool result = (value != INFINITY && value != -INFINITY && value == value);
  return result;
}

static GradientDescentResult do_gradient_descent(Coeffs initial_coeffs, Samples samples, int step_count){
  Coeffs current_coeffs = initial_coeffs;
  
  GradientDescentResult result = { .optimal_coeffs = initial_coeffs, .optimal_cost = calculate_cost(initial_coeffs, samples) };
  
  double *costs = calloc(step_count, sizeof(double));
  
  fjz(step_count){
    Coeffs gradient = calculate_gradient(current_coeffs, samples);
    
    double step_weights[N] = {
      1E-3,
      1E-3,
      1E-4,
      1E-4,
      1E-4,
      1E-4,
      0,
    };
    
    fiz(array_count(gradient.C)){
      current_coeffs.C[i] -= gradient.C[i] * step_weights[i / 2];
    }
    
    double cost = calculate_cost(current_coeffs, samples);
    if(!is_valid_f64(cost)
       || !is_valid_f64(current_coeffs.C[0]) || fabs(current_coeffs.C[0]) > 10000
       || !is_valid_f64(current_coeffs.C[1]) || fabs(current_coeffs.C[1]) > 10000
       || !is_valid_f64(current_coeffs.C[2]) || fabs(current_coeffs.C[2]) > 1000
       || !is_valid_f64(current_coeffs.C[3]) || fabs(current_coeffs.C[3]) > 1000
       || !is_valid_f64(current_coeffs.C[4]) || fabs(current_coeffs.C[4]) > 1000
       || !is_valid_f64(current_coeffs.C[5]) || fabs(current_coeffs.C[5]) > 1000)
    {
      break;
    }
    costs[j] = cost;
    
    int look_back = 512;
    if(j >= look_back){
      double improvement = costs[j - look_back] - cost;
      if(improvement < 1E-5){
        break;
      }
    }
    
    //fprintf(stderr, "%04d: Cost = %f. Gradient is (%E, %E, %E, %E) Current is (%E, %E, %E, %E)\n", j, cost, FCOEFFS(gradient), FCOEFFS(current_coeffs));
    
    result.step_count += 1;
    if(cost < result.optimal_cost){
      result.optimal_cost = cost;
      result.optimal_coeffs = current_coeffs;
    }
  }
  
  free(costs);
  return result;
}

static Samples load_samples(char *path){
  Samples result = {0};
  
  FILE *file = fopen(path, "r");
  if(file){
    bool success = true;
    
    struct{
      char *name;
      int index;
    } fields[] = {
#define FIELD_BACK_DRAG 0
      { "back_tx" },
#define FIELD_FRONT_DRAG 1
      { "front_tx" },
#define FIELD_DISTANCE 2
      { "distance" },
#define FIELD_Y_OFFSET 3
      { "y_offset" },
    };
    
    fprintf(stderr, "Parsing file `%s`...\n", path);
    
    int sample_capacity = 2048;
    result.values = calloc(sample_capacity, sizeof(Sample));
    
    int line_number = 0;
    bool first_line = true;
    size_t line_size = 8192;
    char *line = malloc(line_size);
    while(getline(&line, &line_size, file) >= 0){
      line_number += 1;
      
      char *parts[256] = {0};
      int part_count = split_on_whitespace(parts, array_count(parts), line, true);
      
      if(string_equals(parts[0], "worker")){
        fjz(array_count(fields)){
          fields[j].index = -1;
          
          fiz(part_count){
            if(string_equals(parts[i], fields[j].name)){
              fields[j].index = i;
            }
          }
          if(fields[j].index == -1){
            fprintf(stderr, "Error: Header does not contain field name \"%s\"\n", fields[j].name);
            success = false;
            goto exit_line_loop;
          }
        }
      }else{
        double d = atof(parts[fields[FIELD_DISTANCE].index]);
        if(d > 0){
          Sample sample = {
            ._d = d,
            .log_d = log(d + D_LOG_GLATTNINGSFAKTOR),
            .s = atof(parts[fields[FIELD_Y_OFFSET].index]),
            .gain = 1.0 - (atof(parts[fields[FIELD_BACK_DRAG].index]) / atof(parts[fields[FIELD_FRONT_DRAG].index])),
          };
          
          if(result.count >= sample_capacity){
            fprintf(stderr, "File `%s` contains too many samples!\n", path);
            success = false;
            goto exit_line_loop;
          }
          result.values[result.count++] = sample;
        }else{
          fprintf(stderr, "WARNING: Ignoring sample with small d value! (%s:%d)\n", path, line_number);
        }
      }
      
      first_line = false;
    }exit_line_loop:;
    
    if(!success){
      free(result.values);
      result = (Samples){0};
    }
    
    fclose(file);
  }else{
    fprintf(stderr, "Unable to open the file `%s`\n", path);
  } return result;
}

int main(int argc, char **argv){
  int result = 1;
  if(argc == 2){
    Samples samples = load_samples(argv[1]);

    {
      Coeffs coeffs = {
        .C[0] = 0.539808,
        .C[1] = -0.085378,
        .C[2] = 0.031145,
        .C[3] = -2.411369,
        .C[4] = 0.148270,
        .C[5] = -1.388466,
        .C[6] = D_INV_GLATTNINGSFAKTOR,
        .C[7] = D_LOG_GLATTNINGSFAKTOR
      };
      double cost = calculate_cost(coeffs, samples);
      double max_error = 0;

      double average_error = 0;
      
      fiz(samples.count){
        Sample sample = samples.values[i];
        double approx = f(coeffs, sample);
        
        double error = fabs(sample.gain - approx);
        average_error += error / (double)samples.count;
        if(error > max_error) max_error = error;
        
        //fprintf(stderr, "   [%d] -> %03.3f v %03.3f (%03.3f)\n", i, sample.gain, approx, error);
      }
      double standard_deviation = 0;
      fiz(samples.count){
        Sample sample = samples.values[i];
        double approx = f(coeffs, sample);
        double error = fabs(sample.gain - approx);
        standard_deviation += square(error - average_error);
      }
      standard_deviation = sqrt((double)1 / (samples.count - 1) * standard_deviation);
      fprintf(stderr, "  Maximum error = %f (mean = %f, standard_deviation = %f, cost = %f)\n", max_error, average_error, standard_deviation, cost);

      exit(1);
    }


    if(samples.values){
      
      fiz(samples.count){
        Sample sample = samples.values[i];
        //fprintf(stderr, "Sample[%04d] = %f, %f, %f\n", i, sample.log_d, sample.s, sample.gain);
      }
      
      result = 0;
      
      char *out_path = "data-analysis-output.csv";
      FILE *output = fopen(out_path, "w");
      
      fprintf(stderr, "Analysing the data. Output is being sent to the file `%s`.\n", out_path);
      fprintf(output, "Initial coeffs: 0\t1\t2\t3\tOptimal coeffs 0\t1\t2\t3\tOptimal cost\n");
      fflush(output);
      
      GradientDescentResult best_gd = {0};
      
      time_t start_time = time(NULL);
      time_t end_time = start_time + 30;
      
      int attempt_count = 8192;
      fjz(attempt_count){
        if(end_time <= time(NULL)){
          break;
        }
        Coeffs coeffs = {
          .C[0] = random_f64_ab(-5, 5),
          .C[1] = random_f64_ab(-1, 0),
          .C[2] = random_f64_ab(-1, 1),
          .C[3] = random_f64_ab(-5, 5),
          .C[4] = random_f64_ab(-5, 5),
          .C[5] = random_f64_ab(-5, 5),
          .C[6] = D_INV_GLATTNINGSFAKTOR,
          .C[7] = D_LOG_GLATTNINGSFAKTOR
        };
        
        GradientDescentResult gd = do_gradient_descent(coeffs, samples, 1 << 25);
        if(j == 0 || gd.optimal_cost < best_gd.optimal_cost){
          best_gd = gd;
        }
        
        if(gd.optimal_cost < 0.002){
          fprintf(output,
                  "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t"
                  "%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t"
                  "%f\n",FCOEFFS(coeffs), FCOEFFS(gd.optimal_coeffs), gd.optimal_cost);
          fflush(output);
        }
      }
      fprintf(stderr, "... done!\n");
      
      {
        fprintf(stderr, "best gd: %f in %d steps (coeffs = %f, %f, %f, %f, %f, %f, %f, %f)\n", best_gd.optimal_cost, best_gd.step_count, FCOEFFS(best_gd.optimal_coeffs));
        
        double max_error = 0;

        double average_error = 0;
        
        fiz(samples.count){
          Sample sample = samples.values[i];
          double approx = f(best_gd.optimal_coeffs, sample);
          
          double error = fabs(sample.gain - approx);
          average_error += error / (double)samples.count;
          if(error > max_error) max_error = error;
          
          //fprintf(stderr, "   [%d] -> %03.3f v %03.3f (%03.3f)\n", i, sample.gain, approx, error);
        }
        double standard_deviation = 0;
        fiz(samples.count){
          Sample sample = samples.values[i];
          double approx = f(best_gd.optimal_coeffs, sample);
          double error = fabs(sample.gain - approx);
          standard_deviation += square(error - average_error);
        }
        standard_deviation = sqrt((double)1 / (samples.count - 1) * standard_deviation);
        fprintf(stderr, "  Maximum error = %f (mean = %f, standard_deviation = %f, cost = %f)\n", max_error, average_error, standard_deviation, best_gd.optimal_cost);
      }
      
      fclose(output);
    }else{
      fprintf(stderr, "The samples could not be loaded correctly. Exiting!\n");
    }
    
    free(samples.values);
  }else{
    fprintf(stderr, "Usage: %s <path to output.csv>\n", argv[0]);
  }
  
  return result;
}
