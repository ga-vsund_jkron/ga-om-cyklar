/*
utility.c - code shared between run-jobs.c and create-jobs.c (only those as of 2020-01-29)
Created: 2020-01-29
Author: jkron

*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <time.h>
#include <assert.h>
#include <math.h>
#include <stdarg.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/statvfs.h>
#include <fcntl.h>
#include <sys/types.h>
#include <errno.h>

#include <sys/wait.h>
#include <signal.h>
#include <sys/resource.h>

#include <errno.h>
#include <string.h>

#define array_count(_array_) (sizeof(_array_) / sizeof((_array_)[0]))
#define square(_value_) ((_value_) * (_value_))
#define fiz(_count_) for(int i = 0; i < (_count_); ++i)
#define fjz(_count_) for(int j = 0; j < (_count_); ++j)
#define fkz(_count_) for(int k = 0; k < (_count_); ++k)

#define minimum(_a_, _b_) ((_a_) < (_b_) ? (_a_) : (_b_))
#define maximum(_a_, _b_) ((_a_) > (_b_) ? (_a_) : (_b_))

#define clamp01(_x_) minimum(maximum(0, (_x_)), 1)

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

static bool string_equals(char *a, char *b){
  bool result = false;
  if(a && b){
    result = (strcmp(a, b) == 0);
  }
  return result;
}

static void copy_string(char *dest, size_t dest_cap, char *source){
  assert(dest_cap > strlen(source));
  memcpy(dest, source, strlen(source) + 1);
}

static int split_on_whitespace(char **out, int out_cap, char *line, bool tab_only){
  int out_count = 0;
  
  int begin = 0;
  int line_length = strlen(line);
  for(int i = 0; i < line_length;){
    if((!tab_only && line[i] == ' ') || line[i] == '\t'){
      if(out_count >= out_cap){
        panic("split_on_whitespace exceeded the maximum number of splits.");
      }
      out[out_count++] = line + begin;
      line[i++] = '\0';
      while(line[i] == ' ' || line[i] == '\t') i++;
      begin = i;
    }else{
      i++;
    }
  }
  if(out_count >= out_cap){
    panic("split_on_whitespace exceeded the maximum number of splits when writing the last entry.");
  }
  out[out_count++] = line + begin;
  
  return out_count;
}

static int read_int(char **_str){
  char *str = *_str;
  int result = 0;
  while('0' <= *str && *str <= '9'){ result = result * 10 + (*str - '0'); str++; }
  *_str = str;
  return result;
}

static void fprint_current_date(FILE *file){
  time_t timestamp = time(NULL);
  struct tm *time = localtime(&timestamp);
  
  fprintf(file, "%04d-%02d-%02d_%02d:%02d:%02d", time->tm_year + 1900, time->tm_mon + 1, time->tm_mday, time->tm_hour, time->tm_min, time->tm_sec);
}

static bool file_exists(char *path){
  struct stat buf;
  bool result = (stat(path, &buf) == 0);
  return result;
}

typedef struct IterSpec IterSpec;
struct IterSpec{ /* iteration count specification */
#define ITER_SPEC_fixed 1
#define ITER_SPEC_converge 2
  int kind;
  union{
    int fixed;
    float converge;
  };
};




#define HAS_GETRANDOM_CALL 0

#if HAS_GETRANDOM
#include <sys/random.h>
int getrandom(void *buf, size_t buflen, unsigned int flags);
#else
// One can wonder why they don't include wrappers for all syscalls even if a macro for the syscall number is available...
#include <linux/random.h>
#include <sys/syscall.h>
static int getrandom(void *buf, size_t buflen, unsigned int flags){
  errno = 0;
  int result = syscall(SYS_getrandom, (uint64_t)buf, (uint64_t)buflen, (uint64_t)flags);
  if(errno){
    fprintf(stderr, "Unable to do the getrandom() syscall!\n");
    exit(1);
  }
  return result;
}
#endif

static uint32_t random_u32(){
  uint8_t data[4] = {0};
  fiz(4){
    ssize_t status = getrandom(&data[i], 1, 0);
    int errno_value = errno;
    if(status != 1){
      fprintf(stderr, "[create-jobs.c] Unable to get randomness from the getrandom procedure :-(. getrandom() returned %ld. errno = %d (%s). Shutting down!\n", status, errno_value, strerror(errno_value));
      exit(1);
    }
  }
  uint32_t result = ((uint32_t)data[0] << 0) | ((uint32_t)data[1] << 8) | ((uint32_t)data[2] << 16) | ((uint32_t)data[3] << 24);
  return result;
}

#define rand DONT USE THIS THING USE THE OHTER THING

static double random_f64_01(){
  return ((double)random_u32() / (double)UINT32_MAX);
}

static double random_f64_ab(double a, double b){
  return random_f64_01() * (b - a) + a;
}

static int random_i32_ab(int a, int b){
  assert(b >= a);
  return random_u32() % (b - a + 1) + a;
}
